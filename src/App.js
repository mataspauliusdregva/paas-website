import "./App.css";
import HomePage from "./pages/Homepage";
import { Route } from "react-router-dom/cjs/react-router-dom.min";
import { Redirect, Switch } from "react-router-dom";
import Header from "./components/page-content/topbar/MainHeader";
import Layout from "./components/layout/Layout";
import AuthPage from "./pages/AuthPage";
import ProfilePage from "./pages/Profile";
import PrivateRoute from "./utils/PrivateRoute";
import AuthContext from "../src/context/AuthContext";
import { useContext } from "react";
import Dashboard from "./pages/Dashboard";
import React from 'react'

function App() {
  const authCtx = useContext(AuthContext);
  return (
    <Layout>
      <Header />
      <Switch>
        <Route exact path="/">
          <Redirect to="/home" />
        </Route>
        <Route path="/home">
          <HomePage />
        </Route>
        <Route path="/auth">
          {!authCtx.isLoggedIn ? <AuthPage /> : <Redirect to="/" />}
        </Route>
        <PrivateRoute path="/profile">
          <ProfilePage />
        </PrivateRoute>
        <PrivateRoute path="/dashboard">
          <Dashboard/>
        </PrivateRoute>
        <Route path="*">
          <Redirect to="/" />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
