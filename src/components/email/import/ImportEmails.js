import {
  useHistory,
  useParams,
} from "react-router-dom/cjs/react-router-dom.min";
import { Importer, ImporterField } from "react-csv-importer";
import { useContext, useState } from "react";
import AuthContext from "../../../context/AuthContext";
import { fetchData } from "../../../utils/FetchData";
import { createMultipleEmail } from "../../../constants/Urls";

const ImportEmails = (props) => {
  const history = useHistory();
  const params = useParams();
  const allEmails = []

  const { authTokens } = useContext(AuthContext);
  const authCtx = useContext(AuthContext);

  const success = (data) => {
    authCtx.updateState();
    history.push("/dashboard/emails/" + params.listId);
  };

  const failure = (err) => alert(err.message);

  const createEmailBody = (data, additionalData) => {
    let body;
    if (additionalData !== "") {
      body = {
        email: data,
        information: additionalData,
      };
    } else {
      body = {
        email: data,
        information: {},
      };
    }
    return body;
  };

  const splitData = (data, splitValue) => {
    return data.split(splitValue);
  };

  const convertToJSONObject = (data) => {
    let obj = {};
    data.forEach(function (itm) {
      let split = itm.split(":");
      let key = split[0];
      if (!obj.hasOwnProperty(key)) {
        obj[key] = [];
      }
      obj[key].push(split[1]);
    });
    return obj;
  };

  const createDict = (data) => {
    const dict = {};

    for (const key in data) {
      dict[key] = data[key][0];
    }
    return dict;
  };


  return (
    <Importer
      className={props.class}
      assumeNoHeaders={false}
      restartable={false}
      processChunk={async (rows, { startIndex }) => {
    
        
          allEmails.push(rows)
      
      }}
      onClose={({ file, preview, fields, columnFields }) => {
        let emails = []
        let information = []
        for (let item in allEmails[0]){
          emails.push(allEmails[0][item].email)
          information.push(allEmails[0][item].information)
        }
       
        const body = {
          list_id: params.listId,
          emails: [],
        };

        for (let data in emails) {
          if (emails[data] !== "") {
            if (information[data] !== "") {
              const splittedInformation = splitData(information[data], "/");
              const emailInformation = convertToJSONObject(splittedInformation);
              const formedEmailBody = createEmailBody(
                emails[data],
                createDict(emailInformation)
              );
              body.emails.push(formedEmailBody);
            } else {
              body.emails.push(createEmailBody(emails[data], ""));
            }
          }
        }
    
        fetchData(
          createMultipleEmail,
          {
            "Content-Type": "application/json",
            Authorization: "Bearer " + String(authTokens.access),
          },
          "POST",
          success,
          failure,
          JSON.stringify(body)
        );
      }}
    >
      <ImporterField name="email" label="Email" />
      <ImporterField name="information" label="information" optional />
    </Importer>
  );
};

export default ImportEmails;
