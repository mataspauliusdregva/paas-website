import Email from "../../page-content/forms/email/Email";
import { createEmail } from "../../../constants/Urls";

const CreateEmail = () => {
  return (<Email method = "POST"  type="create" disableButton={true} bin={true} header = "Fill User Data" submitButton = "Create" height = "calc(60vh - 135px)" url={createEmail} />);
};

export default CreateEmail;
