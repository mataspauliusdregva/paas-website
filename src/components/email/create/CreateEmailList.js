import baseClasses from "../../page-content/CSS/Base.module.css";
import { Form, Col, Button } from "react-bootstrap";
import { LinearProgress } from "@material-ui/core";
import { useState, useRef, useContext } from "react";
import classes from "./CreateEmailList.module.css";
import { createList } from "../../../constants/Urls";
import { useHistory } from "react-router-dom";
import AuthContext from "../../../context/AuthContext";
import { fetchData } from "../../../utils/FetchData";
import { xsWidthCreateLists } from "../../../constants/Constants";
import FormGroup from "../../page-content/forms/FormGroup";

const CreateList = () => {

  const nameInputRef = useRef();
  const history = useHistory();

  const [isLoading, setIsLoading] = useState(false);

  const { authTokens } = useContext(AuthContext);
  const authCtx = useContext(AuthContext);
  
  const path = window.location.pathname;

  const resetValues = () =>{
    setIsLoading(false);
    nameInputRef.current.value = "";
  }

  const submitHandler = (event) => {
    event.preventDefault();
    setIsLoading(true);
    const enteredName = nameInputRef.current.value;

    const success = () => {
      resetValues()
      authCtx.updateState();
      history.replace(path);
    };

    const failure = (err) => {
      resetValues()
      alert(err.message);
    };

    fetchData(
      createList,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "POST",
      success,
      failure,
      JSON.stringify({
        name: enteredName,
      })
    );
  };

  return (
    <Col xs={xsWidthCreateLists}>
      <Form
        className={`${baseClasses.form} ${classes.form}`}
        onSubmit={submitHandler}
      >
        <h1 className={baseClasses.h1}>List Information</h1>
        <FormGroup type="name" label="Name" reference={nameInputRef} maxlen = "255"/>
        {!isLoading ? (
          <Button
            className={`${classes.button} ${baseClasses.button}`}
            variant="primary"
            type="submit"
          >
            Submit
          </Button>
        ) : (
          <LinearProgress className={baseClasses.loading} />
        )}
      </Form>
    </Col>
  );
};

export default CreateList;
