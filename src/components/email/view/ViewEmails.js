import { useParams } from "react-router-dom";
import { deleteEmail, deleteEmailMultiple, getEmails } from "../../../constants/Urls";
import Emails from "../../page-content/forms/email/Emails"
import { emailColumns } from "../../page-content/table/Columns";

const ViewEmails = () => {

  const params = useParams()
  const url = "/dashboard/emails/" + params.listId
  return(<Emails url = {getEmails + params.listId} type="emails" columns={emailColumns} parent={false} addUrl = { url+ "/add"} importUrl = {url+ "/import"} deleteUrl={deleteEmail} deleteMultipleUrl={deleteEmailMultiple} flex={1}/>)
};

export default ViewEmails;
