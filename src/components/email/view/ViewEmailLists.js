import Emails from "../../page-content/forms/email/Emails"
import { deleteList, deleteListMultiple, getUserEmailLists } from "../../../constants/Urls"
import { listColumns } from "../../page-content/table/Columns"

const ViewEmailLists = () =>{
    return(<Emails url = {getUserEmailLists} type="emailList" columns={listColumns} parent={true}  addUrl="/dashboard/view_lists/create_list" deleteUrl={deleteList} deleteMultipleUrl={deleteListMultiple} flex={1}/>)
}

export default ViewEmailLists 