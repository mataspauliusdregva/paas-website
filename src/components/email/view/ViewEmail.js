import { updateEmailData } from "../../../constants/Urls";
import Email from "../../page-content/forms/email/Email"

const ViewUser = () => {

  return ( <Email method = "PUT" type="update" bin={true} header = "Update User Data" submitButton = "Update" height = "calc(62vh - 135px)" url={updateEmailData} flex={1}/>
   );
};

export default ViewUser;
