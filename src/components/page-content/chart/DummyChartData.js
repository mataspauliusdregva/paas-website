export const data = [
  {
    name: "Jan",
    clicked: 4000,
  },
  {
    name: "Feb",
    clicked: 5000,
  },
  {
    name: "Mar",
    clicked: 6500,
  },
  {
    name: "Apr",
    clicked: 4500,
  },
  {
    name: "May",
    clicked: 875,
  },
  {
    name: "Jun",
    clicked: 5524,
  },
  {
    name: "Jul",
    clicked: 452,
  },
  {
    name: "Aug",
    clicked: 789,
  },
  {
    name: "Sep",
    clicked: 7778,
  },
  {
    name: "Oct",
    clicked: 6350,
  },
  {
    name: "Nov",
    clicked: 4520,
  },
  {
    name: "Dec",
    clicked: 4000,
  },
];
