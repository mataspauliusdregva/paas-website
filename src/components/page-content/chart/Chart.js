import classes from "./Chart.module.css";
import baseClasses from "../CSS/Base.module.css";
import { Col } from "react-bootstrap";
import { LineChart, Line, XAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { aspectRatio, xsWidthChart } from "../../../constants/Constants";

const Chart = (props) => {
  return (
    <Col className={`${baseClasses.form} ${classes.form}`} xs={xsWidthChart}>
      <h3 className={classes.title}>Analytics</h3>
      <ResponsiveContainer width="100%" aspect={aspectRatio}>
        <LineChart
          data={props.data}
        >
          <CartesianGrid stroke="#e0dfdf" strokeDasharray="5 5"/>
          <XAxis dataKey="month" stroke="var(--additionalColor)"/>
          <Line type="monotone" dataKey="link_opened_times" stroke="var(--secondaryButton)" />
          <Line type="monotone" dataKey="email_opened_times" stroke="var(--primaryButton)" />
          <Tooltip labelStyle={{color: "var(--primary)"}}/>
        </LineChart> 
      </ResponsiveContainer>
    </Col>
  );
};

export default Chart;
