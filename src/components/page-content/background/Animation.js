import {Col} from "react-bootstrap"
import classes from "../background/Animation.module.css"
import Email from "../../resources/lottie/email.json";
import Lottie from "lottie-react";
import React from 'react';

const HomepageAnimation = (props) => {
    return(
        <Col xs={8}>
        <Lottie className={classes.lottie} animationData={Email} style={{height: props.height, width:props.width, bottom: props.bottom, position: props.position}} autoPlay={true} loop={true}/>
      </Col>
    )
}

export default HomepageAnimation