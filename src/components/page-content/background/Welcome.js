import { Fragment } from "react";
import { Col, Button } from "react-bootstrap";
import Typist from "react-typist";
import classes from "../background/Welcome.module.css";
const HomepageDescription = (props) => {
  console.log(window.location.href);
  return (
    <>
      <Col xs={4}>
        <div className={classes.div} >
          <h1>Ensure your bussiness safety</h1>
          <Typist ms={1000} cursor={{ show: false }} >
            <span> Welcome to Fishi👋! </span>
            <Typist.Delay ms={1000} />
            <Typist.Backspace count={20}/>
            <span>
              This is the secure solution to test your co-workers Phishing
              awareness.
            </span>
          </Typist>
          <Button className={classes.button} href="/auth">
            Get Started
          </Button>
        </div>
      </Col>
    </>
  );
};

export default HomepageDescription;
