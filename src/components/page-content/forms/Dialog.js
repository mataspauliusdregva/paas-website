import { Modal, Button } from "react-bootstrap";
import classes from "./Dialog.module.css"

const ResponsiveDialog = (props) => {
  const handleAgree = (event) => {
    event.preventDefault()
    props.handleYes();
    props.handleClose();
  };

  return (
    <>
      <Modal  show={props.open} onHide={props.handleClose} backdrop={false} centered>
        <Modal.Header closeButton>
          <Modal.Title className={classes.modalText}>{props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.text}</Modal.Body>
        <Modal.Footer>
          <Button className={classes.buttonNo} onClick={props.handleClose}>
            {props.noButton}
          </Button>
          <Button className={classes.buttonYes} onClick={handleAgree}>
            {props.yesButton}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};


export default ResponsiveDialog;
