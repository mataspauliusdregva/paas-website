import { Form } from "react-bootstrap";

const FormGroup = (props) => {
  console.log(props.maxlen)
  return (
    <Form.Group className="mb-3" controlId={`form+${props.label}`}>
      <Form.Label>{props.label}</Form.Label>
      <Form.Control type={props.type} placeholder={props.label} ref={props.reference} defaultValue={props.defaultValue} required={true} maxLength={props.maxlen}/>
      {props.children}
    </Form.Group>
  );
};

export default FormGroup;
