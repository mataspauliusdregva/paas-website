import { useContext, useEffect, useCallback, useState } from "react";
import AuthContext from "../../../../context/AuthContext";
import List from "../../table/Table";
import { fetchData } from "../../../../utils/FetchData";
import { xsWidthTable, xsWidthTableFull } from "../../../../constants/Constants";
import EmailListContext from "../../../../context/EmailListContext";
import { useLocation, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { matchPath } from "react-router-dom";

const Emails = (props) => {
  const { authTokens, state } = useContext(AuthContext);
  const emailListCtx = useContext(EmailListContext)

  const [listRows, setListRows] = useState([]);
  const [disableButtons, setDisableButtons] = useState(false)

  const location = useLocation()
  const params = useParams()

  const urls = ["/dashboard/view_lists/create_list", "/dashboard/emails/:id/edit/:itemId", "/dashboard/emails/:id/add" ]
  const width = matchPath(location.pathname, {path: urls})? xsWidthTable : xsWidthTableFull
  

  const failure = (err) => alert(err.message);

  const loadLists = useCallback(() => {
    const success = (data) => {
      const loadedLists = [];
      for (const key in data) {
        if (!isNaN(+key)) {
          if(!props.parent){
            loadedLists.push({
              id: data[key].id,
              email: data[key].email,
              addedAt: data[key].created_at,
              listId: data[key].email_list,
            });
          }
        }
      }
      if(props.parent)
      {
      setListRows(emailListCtx.list)
      }
      else{
      setListRows(loadedLists);
      setDisableButtons( emailListCtx.list.filter((item) => item.id === Number(params.listId))[0].used_in>0)
      }
    };

    fetchData(
      props.url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access, props.url, props.parent, emailListCtx.list, params.listId]);

  useEffect(() => {
    loadLists();
  }, [loadLists, state]);

  return (
    <List
      xs = {width}
      columns={props.columns}
      data={listRows}
      addUrl = {props.addUrl}
      importUrl = {props.importUrl}
      deleteUrl = {props.deleteUrl}
      deleteMultipleUrl = {props.deleteMultipleUrl}
      height={"calc(100vh - 135px)"}
      type={props.type}
      deleteButton={true}
      addButton={true}
      editButton={true}
      checkBoxButton={true}
      flex = {props.flex}
      blurButtons = {disableButtons}
    />
  );
};

export default Emails;
