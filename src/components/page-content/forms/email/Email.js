import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useRef, useState, useContext, useEffect, useCallback } from "react";
import AuthContext from "../../../../context/AuthContext";
import { Form, Button, Col, Row } from "react-bootstrap";
import baseClasses from "../../CSS/Base.module.css";
import Layout from "../../../layout/Layout";
import { LinearProgress } from "@mui/material";
import "react-csv-importer/dist/index.css";
import classes from "./Email.module.css";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import {
  TextField,
  InputLabel,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";
import { universalStyles, stylesForTextField } from "../../../../constants/Styles";
import List from "../../table/Table";
import { userColumns } from "../../../page-content/table/Columns";
import { v4 as uuidv4 } from "uuid";
import { getEmailData } from "../../../../constants/Urls";
import { fetchData } from "../../../../utils/FetchData";
import {listFormData} from "../../../../constants/Misc";
import ImportEmails from "../../../email/import/ImportEmails";
import { newsCategoryId, xsWidthCreateFillUserData, xsWidthUserFormTable } from "../../../../constants/Constants";
import { checkType } from "../../../../constants/Methods";
import { ReactComponent as BulbIcon } from "../../../resources/icons/bulb-icon.svg";
import Popup from "../Popup"
import CSVGuideImage from "../../../resources/images/csv-guide.png"
import FormGroup from "../FormGroup";
import ImportContext from "../../../../context/ImportContext";

const Email = (props) => {
  const history = useHistory();
  const params = useParams();

  const { authTokens } = useContext(AuthContext);
  const authCtx = useContext(AuthContext);

  const emailInputRef = useRef();
  const usernameInputRef = useRef();
  const typeInputRef = useRef();

  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState([]);
  const [usernames, setUsernames] = useState([]);
  const [dict, setDict] = useState([]);
  const [formData, setFormData] = useState([])
  const [usernameErrorText, setUsernameError] = useState("")

  const type = checkType(props.type);
  const path = window.location.pathname;
  const importCtx = useContext(ImportContext)

  const getEmailInformation = useCallback(() => {
    if (!type) {
      const success = (data) => {
        setEmail(data);
        createListFromDict(data.information);
      };

      const failure = (err) => alert(err.message);
      
      fetchData(
        getEmailData + params.listId + "&email_id=" + params.emailId,
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + String(authTokens.access),
        },
        "GET",
        success,
        failure
      );
    }
  }, [authTokens.access, params.listId, params.emailId, setEmail, type]);

  useEffect(() => {
    getEmailInformation();
  }, [getEmailInformation]);

  const createListFromDict = (data) => {
    const list = [];
    const keys = Object.keys(data);
    const values = Object.values(data);
    for (const value in values) {
      list.push({
        id: uuidv4(),
        type: keys[value],
        username: values[value],
      });
    }
    setUsernames(list);
  };

  const createDict = (data) => {
    const dict = {};
    for (const item in data) {
      dict[[data[item].type]] = data[item].username;
    }
    setDict(dict);
  };

  const createBody = (enteredEmail, dict) => {
    let body = JSON.stringify({
      email: enteredEmail,
      list_id: params.listId,
      information: dict,
    });

    if (!type) {
      body = JSON.stringify({
        id: params.emailId,
        email: enteredEmail,
        list_id: params.listId,
        information: dict,
      });
    }
    return body;
  };

  const resetValues = () => {
    setIsLoading(false);
    setDict([]);
    setUsernames([]);
    emailInputRef.current.value = "";
  };

  const openNewWindow = () => {
    if (!type) {
      history.push("/dashboard/emails/" + params.listId);
    } else {
      history.push(path);
    }
  };

  const submitHandler = (event) => {
    event.preventDefault();

    createDict(usernames);

    const enteredEmail = emailInputRef.current.value;
    const body = createBody(enteredEmail, dict);

    setIsLoading(true);

    const success = () => {
      resetValues();
      authCtx.updateState();
      openNewWindow();
    };

    const failure = (err) => {
      setIsLoading(false);
      alert(err.message);
    };

    fetchData(
      props.url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      props.method,
      success,
      failure,
      body
    );
  };

  const handleSameTypes = (data, type, username) =>{
    let uniqueValues = [];
    let exist = false;

    if (data.length >= 1) {
      uniqueValues = data.map((item) => {
        if (item.type === type) {
          exist = true;
          item.username = username;
        }
        return item;
      });
    }
    return [uniqueValues, exist]
  }

  const handleRemoveFromList = (id) => setUsernames(usernames.filter((item) => item.id !== id));
  
  const validateFields = () =>{
    if(!isEmpty(usernameInputRef.current.value))
    {
      setUsernameError("") 
    }
  }

  const isEmpty = (field) =>{
    return field === ""
  }

  const handleAddToList = (event) => {
    event.preventDefault();

    const enteredType = typeInputRef.current.value;
    const enteredUsername = usernameInputRef.current.value;

    if (enteredUsername !== "") {
      let data = handleSameTypes(usernames, enteredType, enteredUsername)[0]
      let exist = handleSameTypes(usernames, enteredType, enteredUsername)[1]

      if (!exist) {
        setUsernames([
          ...usernames,
          { id: uuidv4(), type: enteredType, username: enteredUsername },
        ]);
      } else {
        setUsernames(data);
      }
      usernameInputRef.current.value = "";
    } else {
      setUsernameError("Field is empty!")
    }
  };

  useEffect(() => {
    createDict(usernames);
  }, [setUsernames, usernames]);


  listFormData.then(formData => {
    setFormData(formData)
});
  
  return (
    <Col xs={xsWidthCreateFillUserData}>
      <Form
        className={`${baseClasses.form} ${classes.form}`}
        onSubmit={submitHandler}
        style={{ height: "auto" }}
      >
        <h1 className={baseClasses.h1}>{!importCtx.state ? props.header : "Import Users"} {importCtx.state &&  <Popup
                loadData = {false}
                buttonTitle={<BulbIcon className={classes.icon}/>}
                header={"CSV File example"}
                classes={`${classes.guideButton}`}
                size="md"
              >
                <Row>
                <img alt="" src={CSVGuideImage}></img>
              </Row>
              </Popup>}</h1>
        <Layout>
          {!importCtx.state ? (
            <>
              <FormGroup label="Email" type="email" defaultValue={!type ? email.email : ""} reference={emailInputRef} maxlen = "255"/>
              Username
              <div className={classes.div}>
                <FormControl className={classes.control} sx={universalStyles}>
                  <InputLabel id="demo-simple-select-label">Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Type"
                    defaultValue="Username"
                    inputRef={typeInputRef}
                  >
                    {formData.map((item) => (
                      item.has_username && item.category !== newsCategoryId &&
                      <MenuItem key={item.id} value={item.type}>
                       {item.type}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <TextField
                  className={classes.textfield}
                  name="username"
                  label="Username"
                  variant="filled"
                  inputRef={usernameInputRef}
                  sx={stylesForTextField}
                  onChange={validateFields}
                  error={usernameErrorText !== ""}
                  helperText={usernameErrorText}
                  inputProps={{ maxLength: 150 }}
                />
                <IconButton className={classes.icon} onClick={handleAddToList}>
                  <AddIcon/>
                </IconButton>
              </div>
              <List
                xs = {xsWidthUserFormTable}
                margin={"1rem auto 1rem auto"}
                columns={userColumns}
                data={usernames}
                height={props.height}
                flex = {props.flex}
                deleteButton={true}
                addButton={false}
                editButton={false}
                checkBoxButton={false}
                visualDelete = {true}
                handleListDelete={handleRemoveFromList}
              ></List>
            </>
          ) : (
            <ImportEmails class={classes.importer} createDict={createDict} />
          )}

          {!isLoading ? (
            !importCtx.state && (
              <Button
                className={`${baseClasses.button} ${classes.button}`}
                variant="primary"
                type="submit"
              >
                {props.submitButton}
              </Button>
            )
          ) : (
            <LinearProgress className={`${baseClasses.loading} ${classes.loading}`} />
          )}
          
        </Layout>
      </Form>
    </Col>
  );
};

export default Email;
