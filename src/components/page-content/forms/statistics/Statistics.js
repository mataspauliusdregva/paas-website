import FeaturedInfo from "../../featuredInfo/FeaturedInfo";
import Chart from "../../chart/Chart";
import { Row, Col, Form } from "react-bootstrap";
import classes from "./Statistics.module.css";
import { idle, statisticsBiggerSize, statisticsBiggestSize, statisticsDefaultSize, statisticsLowerSize, xsWidthChart, xsWidthInstanceInfo } from "../../../../constants/Constants";
import ViewTableData from "../../../instance/view/ViewInstanceEmailData";
import { useState, useCallback, useContext, useEffect } from "react";
import { fetchData } from "../../../../utils/FetchData";
import AuthContext from "../../../../context/AuthContext";
import { universalStyles } from "../../../../constants/Styles";
import {
  Autocomplete,
  TextField,
} from "@mui/material";
import baseClasses from "../../CSS/Base.module.css"
import {useParams} from "react-router-dom/cjs/react-router-dom.min";
import InstanceContext from "../../../../context/InstanceContext";


const Statistics = (props) => {
  const [data, setInstances] = useState([]);
  const [instanceSentData, setInstancesSentData] = useState([])
  const [instanceOpenedData, setInstanceOpenedData] = useState([])
  const [defaultSelectValue, setDefaultSelectValue] = useState("")

  const [dates, setDates] = useState([])

  const { authTokens, state } = useContext(AuthContext);
  const {loadState, expand} = useContext(InstanceContext)

  const params = useParams()

  const failure = (err) => console.log(err.message);

  const handleChange = (event) =>{
    const instance = data.filter((item) => ((props.name === "email"? item.email : item.name) === event.target.value))
    setDefaultSelectValue(instance[0])
  }

  const handleInstances = (data) =>{
    const listData = data.filter((item) => (item.used_in>0))
    const instanceData = data.filter((item) => item.status !== idle)
    setDefaultSelectValue(props.name === "list"? listData[0]: props.name === "instance"? instanceData[0] : data[0])
    setInstances(props.name === "list"?listData:data)
  }

  const handleDates = (data) =>{
    const tempData = []
    for (let index=new Date().getMonth()+2; index<=12; index++){
       let formattedDates = {
        month: new Date(index.toString()).toLocaleString('en-us', { month: 'short' }),
        link_opened_times: 0,
        email_opened_times: 0
        }
      tempData.push(formattedDates)
    }
    for (let index=1; index<=new Date().getMonth()+1; index++){
      let formattedDates = {
       month: new Date(index.toString()).toLocaleString('en-us', { month: 'short' }),
       link_opened_times: 0,
       email_opened_times: 0
       }
     tempData.push(formattedDates)
   }

    for(const item in data){

      for(const temp in tempData)
      {
        if(data[item].month === tempData[temp].month && data[item].year === new Date().getFullYear())
        {
          tempData[temp].link_opened_times += Number(data[item].links)
          tempData[temp].email_opened_times += Number(data[item].emails)
        }
      }
    }
    setDates(tempData)
  }

  const loadData = useCallback((url, method) => {
    const success = (data) => {
      method(data)};

    fetchData(
      url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure,
    );
  }, [authTokens.access]);

  useEffect(() => {
    loadData(props.getData, handleInstances);
  }, [loadData, state, loadState, props.getData]);

  useEffect(()=>{
    if(defaultSelectValue || params.instanceId)
    {
    loadData(props.getOpenedData +  (params.instanceId? params.instanceId : props.name === "email"? defaultSelectValue.email:defaultSelectValue.id), setInstanceOpenedData)
    loadData(props.getSentData +  (params.instanceId? params.instanceId : props.name === "email"? defaultSelectValue.email:defaultSelectValue.id), setInstancesSentData)
    loadData(props.getMonthsData +  (params.instanceId? params.instanceId : props.name === "email"? defaultSelectValue.email:defaultSelectValue.id), handleDates)
  }
  }, [loadData, params.instanceId, defaultSelectValue, loadState, props.getMonthsData, props.getSentData, props.getOpenedData, props.name])

  return (
    <Col className={classes.col} xs={xsWidthInstanceInfo}>
      {props.from === "sidebar" && ( <Row className={`${classes.row}`}>
        <Col xs={xsWidthChart} className={classes.col}>
         <Form className={`${baseClasses.form} ${classes.form}`}>
          <p className={classes.para}>Select {props.name}</p>
          <Autocomplete
              className={classes.control}
              id="instances"   
              autoSelect={true}
              options={data.filter((item)=> (item.status !== idle && item.used_in !== 0))}
              value = {defaultSelectValue || ''}
              isOptionEqualToValue={(option, value) => option.id ===  value.id}
              getOptionLabel={option =>(option.used_in !== 0) && (props.name === "email"? option.email || '' : option.name || '')}
              onSelect={handleChange}
              renderInput={(params) => <TextField sx={universalStyles} {...params} label={"Type "+ props.name + " name"}/>}
            />
        </Form> 
        </Col>
        </Row>
      )}
      {!expand &&<>
      <Row>
        <FeaturedInfo data={instanceOpenedData} overallData={instanceSentData} title="Opened" />
      </Row>
      <Row>
        <Chart data={dates}/>
      </Row></>}
      {(defaultSelectValue || params.instanceId) &&
      <Row className={`${classes.row}`}>
        <ViewTableData columns={props.columns} tableUrl={params.instanceId? props.tableUrl + params.instanceId : props.tableUrl + (props.name === "email"? defaultSelectValue.email:defaultSelectValue.id)} height={params.instanceId? (!expand? statisticsDefaultSize: statisticsBiggestSize): !expand? statisticsLowerSize: statisticsBiggerSize}/>
      </Row>}
    </Col>
  );
};

export default Statistics;
