import { TextField } from "@mui/material"
import { universalStyles } from "../../../constants/Styles"

const Fields = (props) =>{
    return(
        <>
        <p className={props.classes.para}>{props.header}</p>
     <TextField
            className={props.classes.textfield}
            id="outlined-basic"
            variant="outlined"
            label={props.label}
            sx={universalStyles}
            inputRef={props.inputRef}
            InputLabelProps={{ shrink: true }}
            inputProps={{ maxLength: props.maxlen }}
          />
        </>
    )
}

export default Fields