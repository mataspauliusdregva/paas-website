export const validateLink = (link, setError) => {
  let url;

  try {
    url = new URL(link);
  } catch (_) {
    setError("Link is empty or format is wrong!");
    return false;
  }
  return url.protocol === "http:" || url.protocol === "https:";
};

export const validateData = (data, text, setError) => {
  if (data === "") {
    setError(text);
    return false;
  }
  return true;
};

export const handleValidation = (data, setError) => {
    if (data !== "") {
      setError("");
    }
  };