import { Col, Row } from "react-bootstrap";
import { useState } from "react";
import { formCategories, listFormData } from "../../../../constants/Misc";
import classes from "./Templates.module.css";

import { universalStyles } from "../../../../constants/Styles";
import { InputLabel, FormControl, Select, MenuItem } from "@mui/material";

const Templates = (props) => {
  const [categoryId, setCategoryId] = useState("");
  const [formData, setFormData] = useState([]);
  const [categories, setCategories] = useState([]);

  listFormData.then((data) => {
    setFormData(data);
  });

  formCategories.then((categories) => {
    setCategories(categories);
  });

  const handleSelect = (event) => {
      props.template(event.target.value);
  };

  const handleLoadTemplate = (event) => {
    const categoryName = event.target.value;
    const category = categories.filter(
      (item) => item.category === categoryName
    );
    setCategoryId(category[0].id);
  };

  return (
    <>
      <Row >
        <Col>
          <span>Select Category</span>
          <FormControl className={classes.control} sx={universalStyles}>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Category"
              onChange={handleLoadTemplate}
              defaultValue=""
            >
              {categories.map((item) => (item.category !== "News" && 
                <MenuItem key={item.id} value={item.category}>
                  {item.category}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Col>
      </Row>
      <Row>
        <Col>
          <span>Select Template</span>
          <FormControl className={classes.control} sx={universalStyles}>
            <InputLabel id="demo-simple-select-label">Template</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Template"
              defaultValue=""
              onChange={handleSelect}
            >
              {formData.map(
                (item) =>
                  item.category === categoryId && (
                    <MenuItem key={item.id} value={item}>
                      {item.type}
                    </MenuItem>
                  )
              )}
            </Select>
          </FormControl>
        </Col>
      </Row>
    </>
  );
};

export default Templates;
