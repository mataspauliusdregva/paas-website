import baseClasses from "../../CSS/Base.module.css";
import classes from "./Instance.module.css";
import { Col, Form, Button, Row } from "react-bootstrap";
import { universalStyles } from "../../../../constants/Styles";
import { useRef, useCallback, useEffect, useContext, useState } from "react";
import { fetchData } from "../../../../utils/FetchData";
import { createInstance, getUserEmailLists, sendEmailWithoutSaving } from "../../../../constants/Urls";
import AuthContext from "../../../../context/AuthContext";
import { TextField } from "@mui/material";
import InstanceStep from "./InstanceStep";
import { InsertDriveFile, Create, Sync } from "@material-ui/icons";
import { xsWidthInstance } from "../../../../constants/Constants";
import { EditorState, convertToRaw } from "draft-js";
import {
  checkType,
  createExampleTemplate,
} from "../../../../constants/Methods";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Popup from "../Popup";
import {
  emailFullRegex,
  usernameRegex,
  emailShortRegex,
  locationRandRegex,
  locationRandCountryRegex,
  ipRandRegex,
  dateNowRegex,
  randNumberRegex,
} from "../../../../constants/Constants";
import draftToHtml from "draftjs-to-html";
import InstanceType from "./InstanceType";
import { Autocomplete } from "@mui/material";

const Instance = (props) => {
  const { authTokens, state, updateState } = useContext(AuthContext);

  const type = checkType(props.type);

  const didMountRef = useRef(false);

  const history = useHistory();

  const [emailLists, setEmailLists] = useState([]);
  const [isBlurred, setIsBlurred] = useState(false);
  const [isSelected, setIsSelected] = useState(false);
  const [editorstate, setEditorState] = useState(EditorState.createEmpty());
  const [enableForm, setEnableForm] = useState(true);
  const [templateData, setTemplateData] = useState([]);
  const [templateState, setTemplateState] = useState(false);
  const [template, setTemplate] = useState("");
  const [isDescBlurred, setIsDescBlurred] = useState(false);
  const [selectedValue, setSelectedValue] = useState("")
  const [generatedMessage, setGeneratedMessage] = useState("")

  const nameInputRef = useRef();
  const descInputRef = useRef();

  const failure = (err) => alert(err.message);

  const getLists = useCallback(() => {
    const success = (data) => {
      const loadedLists = [];
      for (const key in data) {
        if (!isNaN(+key)) {
          loadedLists.push({
            id: data[key].id,
            listName: data[key].name,
            dateCreated: data[key].created_at,
            amount: data[key].email_count,
          });
        }
      }
      setEmailLists(loadedLists);
    };

    fetchData(
      getUserEmailLists,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access]);

  useEffect(() => {
    getLists();
  }, [getLists, state]);

  const handleBlur = (event) => {
    event.preventDefault();
    if (nameInputRef.current.value !== "") {
      setIsBlurred(true);
      enableTypes(enableForm);
    }
  };

  const enableTypes = (data) => {
    if (!data) {
      history.replace("/dashboard/instance/" + props.type);
    }
  };

  const handleChange = (event) => {
    event.preventDefault();
    setIsBlurred(false);
  };

  const handleDesc = (event) => {
    event.preventDefault();
    if (descInputRef.current.value !== "") {
      setIsDescBlurred(true);
    }
  };

  const handleDescChange = (event) => {
    event.preventDefault();
    setIsDescBlurred(false);
  };

  const handleInstanceTypes = () => setEnableForm(false);

  const handleSelect = (event) => {
    event.preventDefault();
    props.enable(true);
    setIsSelected(true);
    setSelectedValue(event.target.value)
  };

  const handleTemplate = useCallback(() => {
    setTemplateState(true);
    setTemplateData(props.template);
  }, [props.template]);

  const handleMessage = useCallback(() => {
    setTemplateState(false);
    setEditorState(EditorState.createWithContent(props.message));
  }, [props.message]);

  const handleGeneratedMessage = useCallback(() => {
    setTemplateState(false);
    setGeneratedMessage(props.generatedMessage);
  }, [props.generatedMessage]);

  useEffect(() => {
    if (didMountRef.current) {
      handleMessage();
    }
    didMountRef.current = true;
  }, [handleMessage]);

  useEffect(() => {
    if (didMountRef.current) {
      handleTemplate();
    }
    didMountRef.current = true;
  }, [handleTemplate]);

  useEffect(() => {
    if (didMountRef.current) {
      handleGeneratedMessage();
    }
    didMountRef.current = true;
  }, [handleGeneratedMessage]);

  const handleReset = () => {
    nameInputRef.current.value = "";
    setEditorState(EditorState.createEmpty());
    setIsBlurred(false);
    setIsSelected(false);
    props.switchState();
    props.resetChoice();
  };

  const success = () => {history.replace("/dashboard/instances/view")
  updateState()} ;

  const handleEnterPress = (event, type) =>{
    if(event.keyCode === 13){ 
      event.preventDefault();
      type === "name"? handleBlur(event) : handleDesc(event)
   }
  }

  const handleSubmit = (event, type) => {
    event.preventDefault();
    const list = emailLists.filter((item)=> item.listName === selectedValue);
    const name = nameInputRef.current.value;
    const description = descInputRef.current.value;
    const url = type ==="save"? createInstance : sendEmailWithoutSaving
    
    const data =
      props.choice === "Template"
        ? templateData.template
        : props.choice === "Generate"? generatedMessage : draftToHtml(convertToRaw(editorstate.getCurrentContent()))

    const body = JSON.stringify({
      name: name,
      list_id: list[0].id,
      title: props.title,
      text: data,
      url: props.url,
      description: description
    });

    fetchData(
      url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "POST",
      success,
      failure,
      body
    );

    if (type) {
      handleReset();
    }
  };

  const handleExampleData = () => {
    const exampleTemplate = createExampleTemplate(
      templateData.template,
      emailFullRegex,
      usernameRegex,
      emailShortRegex,
      locationRandRegex,
      locationRandCountryRegex,
      ipRandRegex,
      dateNowRegex,
      randNumberRegex
    );
    setTemplate(exampleTemplate);
  };

  return (
    <Col xs={xsWidthInstance}>
      <Form className={`${baseClasses.form} ${classes.form}`}>
        <h1 className={baseClasses.h1}>{props.header}</h1>
        <InstanceStep
          row={classes.row}
          header={classes.header}
          id="1."
          col={classes.col}
          color={isBlurred && "var(--primary)"}
          state={isBlurred}
        >
          <TextField
            className={`${classes.materialUIElement}`}
            style={{
              backgroundColor: isBlurred
                ? "var(--primaryLighter)"
                : "var(--primaryButtonDarker)",
            }}
            id="outlined-basic"
            label="Name"
            variant="outlined"
            sx={universalStyles}
            inputRef={nameInputRef}
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue=""
            inputProps={{ maxLength: 255 }}
            onKeyDown={(event) => handleEnterPress(event, "name")}
          />
        </InstanceStep>
        {isBlurred && (
          <InstanceStep
            row={classes.row}
            header={classes.header}
            id="2."
            col={classes.col}
            color={isBlurred && isSelected && "var(--primary)"}
            state={isBlurred && isSelected}
          > 
           <Autocomplete
                style={{
                  backgroundColor: isSelected
                    ? "var(--primaryLighter)"
                    : "var(--primaryButtonDarker)",
                }}

                id="emails"   
                options={emailLists.filter((item)=> item.amount > 0)}
                isOptionEqualToValue={(option, value) => option.id === value.id}
                getOptionLabel={option => option.listName || ''}
                onSelect={handleSelect}
                renderInput={(params) => <TextField sx={universalStyles} {...params} label="Emails"/>}
                 />
          </InstanceStep>
        )}
        {isSelected && isBlurred && type && (
          <InstanceStep
            row={`${classes.row}`}
            header={classes.header}
            id="3."
            col={classes.col}
            color={props.state && "var(--primary)"}
            state={props.state}
          >
            Choose an Email type:
            <InstanceType
              type="Manual"
              choice={props.choice}
              handleInstanceTypes={handleInstanceTypes}
              state={props.state}
              icon={<Create />}
            />
            <InstanceType
              type="Template"
              choice={props.choice}
              handleInstanceTypes={handleInstanceTypes}
              state={props.state}
              icon={<InsertDriveFile />}
            />
            <InstanceType
              type="Generate"
              choice={props.choice}
              handleInstanceTypes={handleInstanceTypes}
              state={props.state}
              icon={<Sync />}
            />
          </InstanceStep>
        )}
        {props.state && isSelected && isBlurred && (
          <InstanceStep
            row={`${classes.row}`}
            header={classes.header}
            id="4."
            col={classes.col}
            color={isDescBlurred && "var(--primary)"}
            state={isDescBlurred}
          >
            <TextField
              className={`${classes.materialUIElement}`}
              style={{
                backgroundColor: isDescBlurred
                  ? "var(--primaryLighter)"
                  : "var(--primaryButtonDarker)",
              }}
              id="outlined-basic"
              label="Description"
              variant="outlined"
              sx={universalStyles}
              inputRef={descInputRef}
              onBlur={handleDesc}
              onChange={handleDescChange}
              inputProps={{ maxLength: 255 }}
              onKeyDown={(event) => handleEnterPress(event, "desc")}
            />
          </InstanceStep>
        )}
        {props.state && isSelected && isBlurred && isDescBlurred && (
          <Row>
            {templateState && (
              <Row>
                <Col>
              <Popup
                loadData={true}
                setExampleData={handleExampleData}
                buttonTitle="Preview"
                header={templateData.type + " Form example"}
                classes={`${baseClasses.button} ${classes.previewButton}`}
              >
                <Col dangerouslySetInnerHTML={{ __html: template }} />
              </Popup></Col>
              </Row>
            )}
            <Row>
            <Col>
            <Button
              className={`${baseClasses.button} ${classes.saveButton}  `}
              variant="primary"
              type="submit"
              onClick={(event) => handleSubmit(event, "save")}
            >
              Save
            </Button>
            </Col>
            <Col>
            <Button
              className={`${baseClasses.button} ${classes.mainButton}`}
              variant="primary"
              type="submit"
              onClick={(event) => handleSubmit(event, "send")}
            >
              Send
            </Button>
            </Col>
            </Row>
          </Row>
        )}
      </Form>
    </Col>
  );
};

export default Instance;
