import { Form, Col, Button, Row } from "react-bootstrap";
import baseClasses from "../../../page-content/CSS/Base.module.css";
import classes from "./CreateInstance.module.css";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ContentState, EditorState } from "draft-js";
import { useTour } from "@reactour/tour";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { newsCategoryId, plainTypeId, xsWidthCreateInstance } from "../../../../constants/Constants";
import { AdditionalInformation } from "../../tour/AdditionalInformation";
import { universalStyles } from "../../../../constants/Styles";
import { TextField } from "@mui/material";
import Templates from "./Templates";
import GuideContext from "../../../../context/GuideContext";
import { ArrowDropDown, ArrowDropUp } from "@material-ui/icons";
import { validateData, validateLink, handleValidation } from "../Validation";
import Popup from "../Popup";
import draftToHtml from "draftjs-to-html";
import { convertToRaw } from "draft-js";
import {
  emailFullRegex,
  usernameRegex,
  emailShortRegex,
  locationRandRegex,
  locationRandCountryRegex,
  ipRandRegex,
  dateNowRegex,
  randNumberRegex,
  urlRegex,
  textInUrlRegex
} from "../../../../constants/Constants";

import { createExampleTemplate } from "../../../../constants/Methods";
import { ArrowForwardIos, ArrowBackIos } from "@material-ui/icons";
import { InputLabel, FormControl, Select, MenuItem } from "@mui/material";
import { listFormData } from "../../../../constants/Misc";
import { fetchData } from "../../../../utils/FetchData";
import { getAllTags, instanceGenerate } from "../../../../constants/Urls";
import AuthContext from "../../../../context/AuthContext";
import htmlToDraft from "html-to-draftjs";
import { stateFromHTML } from 'draft-js-import-html'

const CreateInstance = (props) => {
  const history = useHistory();

  const guideSteps = props.tourSteps ? props.tourSteps : [];

  const linkInputRef = useRef();
  const titleInputRef = useRef();
  const [generatedMessages, setGeneratedMessage] = useState([]);

  const [editorstate, setEditorState] = useState(EditorState.createEmpty());
  const [generatedMessageId, setGeneratedMessageId] = useState(0);
  const [blurPrevButton, setBlurPrevButton] = useState(true);
  const [blurNextButton, setBlurNextButton] = useState(true);
  const [template, setTemplate] = useState("");
  const [error, setError] = useState("");
  const [linkError, setLinkError] = useState("");
  const [show, setShow] = useState(false);
  const [preview, setPreviewData] = useState("");
  const [category, setCategory] = useState("war");
  const [categories, setCategories] = useState([])
  const [type, setType] = useState(20)
  const [formData, setFormData] = useState([])

  const tourCtx = useContext(GuideContext);
  const {authTokens} = useContext(AuthContext)

  const { setIsOpen } = useTour();

  listFormData.then((data) => {
    setFormData(data);
  });
  
  const handleGenerate = (event) => {
    event.preventDefault();
    const data = formData.filter((item) => item.id === type);
  
    const body = JSON.stringify({
     tag: category,
     text: data[0].template
    })
    
    const success = (data) => {
      titleInputRef.current.value = data.title
      generatedMessages.push({"text": data.text, "type": type, "title": data.title});
      setGeneratedMessageId(generatedMessages.length - 1);
      const contentBlock = htmlToDraft(generatedMessages[generatedMessages.length - 1]["text"])
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks, contentBlock.entityMap);
      const editorState = EditorState.createWithContent(contentState);
      setEditorState(editorState);
    }
  
    fetchData(
      instanceGenerate,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "POST",
      success,
      failure, 
      body);
    
  };

  const handleLoadCategory = (event) => {
    const categoryName = event.target.value;
    const category = categories.filter(
      (item) => item.tag === categoryName
    );
    setCategory(category[0].tag);
  };

  const handleLoadType = (event) => {
    const typeName = event.target.value;
    const type = formData.filter(
      (item) => item.id === typeName
    );
    setType(type[0].id);
  };

  const handleButtonBlur = useCallback(() => {
    if (generatedMessageId - 1 >= 0) {
      setBlurPrevButton(false);
    }
    if (generatedMessageId + 1 < generatedMessages.length) {
      setBlurNextButton(false);
    }
    if (generatedMessageId + 1 >= generatedMessages.length) {
      setBlurNextButton(true);
    }
    if (generatedMessageId - 1 < 0 || generatedMessages.length === 1) {
      setBlurPrevButton(true);
    }
  }, [generatedMessages, generatedMessageId]);

  const handleNext = (event) => {
    event.preventDefault();
    let id = generatedMessageId + 1;
    setGeneratedMessageId(id);
    const editorState = EditorState.createWithContent(stateFromHTML(generatedMessages[id]["text"]));
    titleInputRef.current.value = generatedMessages[id]["title"]
    setEditorState(editorState);
  };

  const handlePrev = (event) => {
    event.preventDefault();
    let id = generatedMessageId - 1;
    const editorState = EditorState.createWithContent(stateFromHTML(generatedMessages[id]["text"]));
    titleInputRef.current.value = generatedMessages[id]["title"]
    setEditorState(editorState);
    setGeneratedMessageId(id);
  };

  useEffect(() => {
    handleButtonBlur();
  }, [handleButtonBlur]);

  const onEditorStateChange = (editorState) => setEditorState(editorState);
  const acceptInput = () => {
    props.switchState();
    history.replace("/dashboard/instance/create");
  };



  const handleSubmit = (event) => {
    event.preventDefault();
    const phishingUrl = linkInputRef.current.value;
    if (validateLink(phishingUrl, setLinkError)) {
      if (props.type !== "Template") {
        
        const title = titleInputRef.current.value;
        if (validateData(title, "Subject is empty!", setError)) {
          props.type === "Generate"? props.generatedMessage(generatedMessages[generatedMessageId]["type"] === plainTypeId? draftToHtml(convertToRaw(editorstate.getCurrentContent())) : generatedMessages[generatedMessageId]["text"]) : props.message(editorstate.getCurrentContent());
          props.title(title);
          props.url(phishingUrl);
          props.choice(props.type);
          acceptInput();
        }
      } else {
        if (validateData(template, "You didn't choose a template!", alert)) {
          props.template(template);
          props.link(phishingUrl);
          props.url(phishingUrl);
          props.title(template.title);
          props.choice(props.type);
          acceptInput();
        }
      }
    }
  };

  const handleTour = useCallback(
    () => setIsOpen(tourCtx.guideState),
    [setIsOpen, tourCtx]
  );
  const getTemplate = (template) => setTemplate(template);

const failure = (err) => alert(err.message);

 const handleGetAllTags = useCallback(() =>{
  const success = (data) => {
    setCategories(data)
  }

  fetchData(
    getAllTags,
    {
      "Content-Type": "application/json",
      Authorization: "Bearer " + String(authTokens.access),
    },
    "GET",
    success,
    failure,
  );
  }, [authTokens.access])

  useEffect(() => {
    handleTour();
  }, [handleTour]);

  useEffect(() =>{
    handleGetAllTags()
  }, [handleGetAllTags])

  const handleShow = () => {
    setShow((prevState) => !prevState);
  };

  const handlePreview = (data) => {
    if (data !== undefined) {
      const exampleTemplate = createExampleTemplate(
        data,
        emailFullRegex,
        usernameRegex,
        emailShortRegex,
        locationRandRegex,
        locationRandCountryRegex,
        ipRandRegex,
        dateNowRegex,
        randNumberRegex,
        urlRegex,
        textInUrlRegex
      );
      setPreviewData(exampleTemplate);
    }
  };

  return (
    <Col xs={xsWidthCreateInstance}>
      <Form className={`${baseClasses.form} ${classes.form} ${guideSteps[0]}`}>
        <h1 className={baseClasses.h1}>{props.header}</h1>
        {props.type !== "Template" ? (
          <>
            <Button
              className={`${baseClasses.button} ${classes.showButton}`}
              onClick={handleShow}
            >
              <Row className={guideSteps[1]}>
                <Col xs={1}>{show ? <ArrowDropUp /> : <ArrowDropDown />}</Col>
                <Col xs={10}>Commands</Col>
              </Row>
            </Button>
            {show && (
              <AdditionalInformation
                classesPara={`${classes.para}`}
                classes={classes.tags}
                btnClass={classes.guideButton}
              />
            )}
          </>
        ) : (
          <Templates
            buttonClasses={classes.templateButton}
            template={getTemplate}
          />
        )}
        <p className={classes.span}>Phishing URL</p>
        <TextField
          className={`${classes.field} ${
            props.type !== "Template" ? guideSteps[2] : guideSteps[1]
          }`}
          id="outlined-basic"
          label="URL"
          variant="outlined"
          error={props.update ? props.linkError !== "" : linkError !== ""}
          helperText={props.update ? props.linkError : linkError}
          sx={universalStyles}
          inputRef={props.update ? props.urlInputRef : linkInputRef}
          InputLabelProps={{ shrink: props.update }}
          onChange={() =>
            handleValidation(
              props.update
                ? props.urlInputRef.current.value
                : linkInputRef.current.value,
              props.update ? props.setLinkError : setLinkError
            )
          }
        />
        {props.type !== "Template" && (
          <>
            { <><p className={classes.span}>Message Subject</p>
            <TextField
              className={`${classes.field} ${guideSteps[3]}`}
              id="outlined-basic"
              label="Subject"
              variant="outlined"
              sx={universalStyles}
              inputRef={props.update ? props.subjectInputRef : titleInputRef}
              error={props.update ? props.error !== "" : error !== ""}
              helperText={props.update ? props.error : error}
              InputLabelProps={{ shrink: props.type === "Generate" || props.update}}
              inputProps={{ maxLength: 255 }}
              onChange={() =>
                handleValidation(
                  props.update
                    ? props.urlInputRef.current.value
                    : titleInputRef.current.value,
                  props.update ? props.setError : setError
                )
              }
            /></>}
            {props.type === "Generate" && (
              <Row>
                <Col>
                  <Button
                    className={`${baseClasses.button} ${classes.generateButton} ${classes.arrowPrev} `}
                    variant="primary"
                    type="submit"
                    onClick={handlePrev}
                    disabled={blurPrevButton}
                  >
                    <ArrowBackIos />
                    Prev
                  </Button>
                </Col>
                <Col>
                  <Button
                    className={`${baseClasses.button} ${classes.generateButton} ${classes.arrowNext}`}
                    variant="primary"
                    type="submit"
                    onClick={handleNext}
                    disabled={blurNextButton}
                  >
                    Next <ArrowForwardIos />
                  </Button>
                </Col>
              </Row>
            )}
           { ((generatedMessages.length> 0 && generatedMessages[generatedMessageId]["type"] === plainTypeId) || (props.type === "Manual")) && <div className={`${classes.editor} ${guideSteps[4]}`}>
              <Editor
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName={
                  show
                    ? props.type === "Generate"
                      ? classes.editGenerateScreen
                      : classes.editOtherScreen
                    : props.type === "Generate"
                    ? classes.editExpandedGenerateScreen
                    : classes.editExpandedOtherScreen
                }
                editorState={props.update ? props.editorState : editorstate}
                onEditorStateChange={
                  props.update ? props.editorStateChange : onEditorStateChange
                }
              ></Editor>
            </div>}
          </>
        )}
        {
          <Row>
            <Col>
              <Popup
                buttonTitle="Preview"
                classes={`${baseClasses.button} ${classes.buttonPreview}`}
                header={"Message preview"}
                loadData={true}
                setExampleData={() =>
                  handlePreview(
                    props.type === "Template"
                      ? template.template
                       : props.update
                      ? draftToHtml(
                          convertToRaw(props.editorState.getCurrentContent())
                        )
                      : props.type === "Generate"? (generatedMessages[generatedMessageId]["type"] === plainTypeId? draftToHtml(convertToRaw(editorstate.getCurrentContent())) : generatedMessages[generatedMessageId]["text"]) : draftToHtml(convertToRaw(editorstate.getCurrentContent())))
                }
              >
                <Row dangerouslySetInnerHTML={{ __html: preview }} />
              </Popup>
            </Col>
            {!props.update && (
              <>
                {props.type === "Generate" && (
                  <Col>
                    <Popup
                      classes={`${baseClasses.button} ${classes.generateButton} ${guideSteps[5]}`}
                      buttonTitle="Generate"
                      header={"Choose a type"}
                      additionalButton = {true}
                      additionalButtonClasses = {`${baseClasses.button} ${classes.button}`}
                      additionalButtonMethod = {(event) => handleGenerate(event)}
                      additionalButtonTitle = "Submit"
                      formClass = {`${baseClasses.form}`}
                    >
                      <Row>
                      <Col>
                          <span>Select Type</span>
                          <FormControl
                            className={classes.control}
                            sx={universalStyles}
                          >
                            <InputLabel id="demo-simple-select-label">
                              Type
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              label="Type"
                              onChange={handleLoadType}
                              defaultValue = {type || ''}
                              value =  {type || ''}
                            >
                               {formData.map(
                                (item) => item.category === newsCategoryId && (
                                    <MenuItem key={item.id} value={item.id}>
                                      {item.type}
                                    </MenuItem>
                                  )
                              )}
                            </Select>
                          </FormControl>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <span>Select Category</span>
                          <FormControl
                            className={classes.control}
                            sx={universalStyles}
                          >
                            <InputLabel id="demo-simple-select-label">
                              Category
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              label="Category"
                              onChange={handleLoadCategory}
                              defaultValue = {category || ''}
                              value =  {category || ''}
                            >
                              {categories.map((item) => (
                                <MenuItem key={item.id} value={item.tag}>
                                  {item.tag}
                                </MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                        </Col>
                      </Row>
                    </Popup>
                  </Col>
                )}
                <Col>
                  <Button
                    className={`${baseClasses.button} ${classes.button} ${guideSteps[5]}`}
                    variant="primary"
                    type="submit"
                    onClick={handleSubmit}
                  >
                    Submit
                  </Button>
                </Col>
              </>
            )}
          </Row>
        }
      </Form>
    </Col>
  );
};

export default CreateInstance;
