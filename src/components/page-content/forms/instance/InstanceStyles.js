export const checkboxStyles = {
    margin: "1.46rem",
    marginLeft: "1rem",
    transform: "scale(3)",
    color: "white",
    "&.Mui-checked": {
      color: "white",
    }, 
}