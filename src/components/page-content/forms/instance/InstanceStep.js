import { Row, Col } from "react-bootstrap";
import { checkboxStyles } from "./InstanceStyles";
import { FormControlLabel, Checkbox } from "@mui/material";
import {
  xsWidthInstanceFormRowCheckbox,
  xsWidthInstanceFormRowInput,
  xsWidthInstanceFormRowNumber,
} from "../../../../constants/Constants";

const InstanceStep = (props) => {
  return (
    <Row className={props.row} style={{ backgroundColor: props.color }}>
      <Col xs={xsWidthInstanceFormRowNumber}>
        <h1 className={props.header}>{props.id}</h1>
      </Col>
      <Col className={`${props.col}`} xs={xsWidthInstanceFormRowInput}>
        {props.children}
      </Col>
      <Col xs={xsWidthInstanceFormRowCheckbox}>
        <FormControlLabel
          control={<Checkbox checked={props.state} sx={checkboxStyles} />}
          label=""
        />
      </Col>
    </Row>
  );
};

export default InstanceStep;
