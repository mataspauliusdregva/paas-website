import { Link } from "react-router-dom"
import { Button } from "react-bootstrap"
import baseClasses from "../../CSS/Base.module.css";
import classes from "./InstanceType.module.css";

const InstanceType = (props) =>{
    return(
        <Link
        to={
          !props.state
            ? "/dashboard/instance/create/"+props.type.toLowerCase()
            : "/dashboard/instance/create"
        }
      >
        <Button
          id = {props.type}
          onClick={props.handleInstanceTypes}
          className={`${baseClasses.button} ${classes.button} ${props.choice === props.type && classes.selectedButton}`}
          disabled={props.state}
        >
          {props.icon} {props.type}
        </Button>
      </Link>
    )
}

export default InstanceType