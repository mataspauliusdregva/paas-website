import GuideContext from "../../../context/GuideContext";
import { useContext, useState } from "react";
import { Button, Modal, Col, Form } from "react-bootstrap";
import classes from "./Popup.module.css"

const Popup = (props) =>{

    const [show, setShow] = useState(false);
    const tourCtx = useContext(GuideContext)

    const handleClose = () => setShow(false);
    const handleShow = () => {
        props.loadData && props.setExampleData()
        tourCtx.guideSetState(false)
        setShow(true)};
  
    const handleAdditionalButton = (event) =>{
      event.preventDefault()
      props.additionalButtonMethod(event)
      handleClose()
    }
  
    return (
        <>
        <Button className={props.classes} onClick={handleShow}>{props.buttonTitle}</Button>
        <Modal
          size={props.size? props.size : "lg"}
          show={show}
          aria-labelledby="contained-modal-title-vcenter"
          onHide={handleClose}
          className={ classes.bodyWithoutWidth}
        >{!props.additionalButton &&
          <Modal.Header className={classes.header} closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {props.header}
            </Modal.Title>
          </Modal.Header>}
          <Modal.Body  className={`${props.size? classes.body : ""} ${props.additionalButton && classes.bodyWithoutWidthAndBackground}`} ><Col>{props.additionalButton? <Form className={props.formClass}>{props.children} <Button className={props.additionalButtonClasses} onClick={handleAdditionalButton}>{props.additionalButtonTitle}</Button></Form>: props.children}</Col></Modal.Body>
        </Modal>
      </>
    );
}

export default Popup