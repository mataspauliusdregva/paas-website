import { Check, Close } from "@material-ui/icons";
import classes from "./Columns.module.css"

const formatDate = (date) =>{
  const fullYear = date.split("T")[0]
  const time = date.split("T")[1].split(".")[0]

  return fullYear+" "+ time
}

export const listColumns = {
  items: [
    {
      field: "listName",
      headerName: "List name",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "dateCreated",
      headerName: "Created",
      flex: 2,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? formatDate(params.value): "-";
      },
    },
    {
      field: "amount",
      headerName: "Email Amount",
      flex: 1,
      headerAlign: "center",
      align: "center",
    }
  ],
};

export const emailColumns = {
  items: [
    {
      field: "email",
      headerName: "Email",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "addedAt",
      headerName: "Added at",
      flex: 2,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? formatDate(params.value): "-";
      },
    },
  ],
};

export const userColumns = {
  items: [
    {
      field: "type",
      headerName: "Type",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "username",
      headerName: "Username",
      flex: 3,
      headerAlign: "center",
      align: "center",
    },
  ],
};

export const instanceColumns = {
  items: [
    {
      field: "name",
      headerName: "Name",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "status_name",
      headerName: "Status",
      flex: 1,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "email_list_name",
      headerName: "List name",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "created_at",
      headerName: "Created At",
      flex: 2,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? formatDate(params.value): "-";
      },
    },
  ],
};

export const emailStatisticsColumns = {
  items: [
    {
      field: "email",
      headerName: "Email",
      flex: 3,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "opened_email_times",
      headerName: "Email Opened",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "opened_link_times",
      headerName: "Link Opened",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "sent_date",
      headerName: "Sent date",
      flex: 2,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? formatDate(params.value): "-";
      },
    },
    {
      field: "sent",
      headerName: "Sent",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
    {
      field: "opened_email",
      headerName: "Email Opened",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
    {
      field: "opened_link",
      headerName: "Link Opened",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
  ],
};

export const instanceStatisticsColumns = {
  items: [
    {
      field: "instance_name",
      headerName: "Instance name",
      flex: 3,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "opened_email_times",
      headerName: "Email Opened",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "opened_link_times",
      headerName: "Link Opened",
      flex: 2,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "sent_date",
      headerName: "Sent date",
      flex: 2,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? formatDate(params.value): "-";
      },
    },
    {
      field: "sent",
      headerName: "Sent",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
    {
      field: "opened_email",
      headerName: "Email Opened",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
    {
      field: "opened_link",
      headerName: "Link Opened",
      flex: 1,
      headerAlign: "center",
      align: "center",
      renderCell: (params) => {
        return params.value? <Check className={classes.positive}/> : <Close className={classes.negative}/>;
      },
    },
  ],
};