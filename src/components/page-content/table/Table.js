import { DataGrid } from "@mui/x-data-grid";
import "./Table.module.css";
import classes from "./Table.module.css";
import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import {
  GridToolbarContainer,
  GridToolbarExport,
  GridToolbarFilterButton,
} from "@mui/x-data-grid";
import { Delete, Add, ImportExport, Description, Fullscreen, FullscreenExit} from "@material-ui/icons";
import AuthContext from "../../../context/AuthContext";
import { listStyles } from "./TableStyles";
import {
  useHistory,
  useParams,
} from "react-router-dom/cjs/react-router-dom.min";
import { fetchData } from "../../../utils/FetchData";
import { Col } from "react-bootstrap";
import EmailListContext from "../../../context/EmailListContext";
import { CircularProgress } from "@mui/material";
import ImportContext from "../../../context/ImportContext";
import {
  finishedStatus,
  idle,
  inProgress as inProgressStatus,
} from "../../../constants/Constants";
import Popup from "../forms/Popup";
import InstanceContext from "../../../context/InstanceContext";
import { Refresh } from "@material-ui/icons";
import ResponsiveDialog from "../forms/Dialog";

const Table = (props) => {
  const { authTokens } = useContext(AuthContext);
  const emailListCtx = useContext(EmailListContext);
  const importCtx = useContext(ImportContext);
  const {setState, setEmailListState, setExpand, expand} = useContext(InstanceContext)

  let path = window.location.pathname + "/";
  const history = useHistory();
  const params = useParams();

  const [open, setOpen] = useState(false);
  const [data, setData] = useState(props.data);
  const [selectedList, setSelectedList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [deleteId, setDeleteId] = useState("")

  const showImportForm = () => {
    importCtx.setState(true);
  };

  const hideImportForm = () => {
    importCtx.setState(false);
  };

  const handleRefresh = () =>{
    setEmailListState()
    setState()
  }

  const handleClickOpen = (id) => {
    setDeleteId(id)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const customToolbar = () => {
    return (
      <GridToolbarContainer>
        <GridToolbarFilterButton />
        <GridToolbarExport />
        {props.addButton && !props.blurButtons &&(
          <Link to={props.addUrl} onClick={hideImportForm}>
            <button className={classes.addButton}>
              <Add className={classes.plusButton} />
              Add
            </button>
          </Link>
        )}
        {props.type === "emails" && !props.blurButtons && (
          <Link to={props.addUrl}>
            <button className={classes.addButton} onClick={showImportForm}>
              <ImportExport className={classes.plusButton} />
              import
            </button>
          </Link>
        )}
        {props.refreshButton && (<button className={classes.addButton} onClick={handleRefresh}>
         <Refresh/> Refresh
        </button>)}
        {props.expandButton && (<button className={classes.addButton} onClick={handleExpand}>
         {!expand? <><Fullscreen/>Expand </>:  <><FullscreenExit/>Shrink</>}
        </button>)}
      </GridToolbarContainer>
    );
  };

  if (props.type === "emailList") {
    path = "/dashboard/emails/";
  }

  if (path.endsWith("/add/")) {
    path = path.slice(0, -4);
  }

  if (path.includes("/edit/")) {
    path = path.substring(0, path.indexOf("edit/"));
  }


  const handleOpenWindow = (link) =>{
    importCtx.setState(false)
    history.push(link)
  }

  const handleExpand = () =>setExpand()

  const columns = {
    items: [
      {
        field: "action",
        headerName: "Action",
        headerAlign: "center",
        align: "center",
        flex: props.flex,

        renderCell: (params) => {
          return (
            <>
              {props.type === "instance" &&
                (!isLoading ? (
                  <button
                    disabled={
                      params.row.status === finishedStatus ||
                      params.row.status === inProgressStatus
                    }
                    onClick={() => handleSendMail(params.row.id)}
                    className={classes.sendButton}
                  >
                    Send
                  </button>
                ) : (
                  <CircularProgress className={classes.loading} />
                ))}

              {props.type === "instance" && props.editButton && !isLoading &&(
                  <button  disabled={
                      params.row.status === finishedStatus ||
                      params.row.status === inProgressStatus
                    } 
                    className={classes.editButton} onClick={()=>handleOpenWindow(path + params.row.id + "/edit")}>Edit</button>
              )}

              {props.editButton && !isLoading && (
                  <button className={classes.editButton} disabled={params.row.status === idle || props.blurButtons} onClick={() => handleOpenWindow( props.type !== "emails"? path + params.row.id: path + "edit/" + params.row.id)}>
                    {props.type === "instance" ? "View" : "Edit"}
                  </button>
              )}

              {props.descriptionButton && 
                <Popup
                loadData = {false}
                buttonTitle={<Description style={{backgroundColor: "var(--primaryColor)"}} />}
                header={"Description"}
                classes = {classes.descButton}
              >
                  {params.row.description}
                </Popup>} 
        
              {props.deleteButton && (
                <>

                <ResponsiveDialog open={open} handleClose={handleClose} title = {"Are you sure?"}  text = {`Do you really want to delete this item? This process cannot be undone.`} yesButton="Delete" noButton="Cancel" handleYes={() =>
                  confirmDelete()}/>
                <button
                  className={classes.deleteButton}
                  disabled={params.row.status === inProgressStatus || props.blurButtons}
                  onClick={() => handleClickOpen(params.row.id)}
                >
                  <Delete
                    className={ 
                      (params.row.status === inProgressStatus || props.blurButtons)
                        ? classes.deleteIconDisabled
                        : classes.deleteIcon
                    }
                  />
                </button></>
              )}
            </>
          );
        },
      },
    ],
  };

  const list = props.actions? {items: props.columns.items} : { items: [...props.columns.items, ...columns.items] };

  const confirmDelete = () =>{
    !props.visualDelete? handleItemDelete(deleteId): handleListDelete(deleteId)
  }

  const failure = (err) => {
    alert(err.message);
    setIsLoading(false);
  };

  const handleSendMail = (id) => {
    const body = JSON.stringify({
      instance_id: id,
    });

    const success = () => {
      setIsLoading(false);
      setState()
    };

    setIsLoading(true);

    fetchData(
      props.sendUrl,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "POST",
      success,
      failure,
      body
    );
  };

  const setSelectedListHandler = (data) => {
    const list = [];
    list.push(data);
    setSelectedList(list);
  };

  const openNewWindow = () => {
    if (props.type === "emailList") {
      history.replace("/dashboard/view_lists/");
      emailListCtx.fetchList();
    } else {
      emailListCtx.fetchList();
      history.replace(path.slice(0, -1));
    }
  };

  useEffect(() => {
    setData(props.data);
  }, [props.data]);

  const handleListDelete = (id) => {
    setData(data.filter((item) => item.id !== id));
    props.handleListDelete(id);
  };

  const handleItemDelete = (id) => {
    let url;
    let body;
    let success;

    const failure = (err) => alert(err.message);

    if (selectedList.length === 0 || selectedList[0].length === 0) {
      url = props.deleteUrl;

      body = JSON.stringify({
        id: id,
      });

      success = () => {
        setData(data.filter((item) => item.id !== id));
        openNewWindow();
      };
    } else {
      url = props.deleteMultipleUrl;
      body = JSON.stringify({
        ids: selectedList[0],
        list_id: params.listId,
      });

      if (props.type !== "emails") {
        body = JSON.stringify({
          ids: selectedList[0],
        });
      }

      success = () => {
        setData(data.filter((item) => !selectedList[0].includes(item.id)));
        openNewWindow();
      };
    }

    fetchData(
      url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "DELETE",
      success,
      failure,
      body
    );
  };

  return (
    <Col xs={props.xs} className={props.removePadding}>
      <DataGrid
        className={classes.table}
        style={{ height: props.height, width: props.width}}
        rows={data}
        disableSelectionOnClick
        columns={list.items}
        autoPageSize
        checkboxSelection={props.checkBoxButton}
        onSelectionModelChange={(newSelection) => {
          setSelectedListHandler(newSelection);
        }}
        components={{ Toolbar: customToolbar }}
        {...data}
        sx={listStyles}
      />
    </Col>
  );
};

export default Table;
