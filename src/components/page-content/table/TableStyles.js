export const listStyles = {
  "& .MuiTablePagination-displayedRows": {
    color: "var(--primary)",
  },

  "& .MuiDataGrid-cell:active": {
    color: "var(--primary)",
  },
  "& .MuiSvgIcon-root": {
    color: "var(--secondary)",
  },
  "& .MuiDataGrid-row:nth-of-type(odd).Mui-selected": {
    backgroundColor: "#A6B1E1 !important",
    color: "white !important",
    "& .MuiSvgIcon-root": {
      color: "var(--secondary)",
    },
  },
  "& .MuiDataGrid-row:nth-of-type(even).Mui-selected": {
    backgroundColor: "#A6B1E1 !important",
    color: "white !important",
    "& .MuiSvgIcon-root": {
      color: "var(--secondary)",
    },
  },
  "& .MuiButton-text": {
    color: "var(--primary)",
  },
  "& .MuiDataGrid-toolbarContainer": {
    width: "auto",
    display: "flex",
    flex: "1",
  },
  "& .MuiDataGrid-columnHeaders": {
    background: "var(--primaryButton) !important",
    border: 1,
    borderColor:"var(--secondary) !important",
    color: "white",
  },
  "& .MuiDataGrid-row:nth-of-type(odd)": {
    background: "white !important",
    
  },
  "& .MuiDataGrid-row:nth-of-type(even)": {
    backgroundColor: "var(--additionalColor)",
    border: 1,
  },
  "& .MuiDataGrid-row":{
    borderColor:"var(--secondary) !important",
    color:"var(--secondary) !important",
  },
  "& .MuiDataGrid-cell": {
    outline: "none !important",
  },

  
  
};
