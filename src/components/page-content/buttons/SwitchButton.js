import { Button } from "react-bootstrap";

const SwitchButton = (props) => {
  return (
    <Button
      type="button"
      className={props.classes}
      onClick={props.switchModeHandler}
    >
      {props.state ? props.trueValue : props.falseValue}
    </Button>
  );
};

export default SwitchButton;
