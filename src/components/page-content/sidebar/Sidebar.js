import React from "react";
import classes from "./Sidebar.module.css";
import "./Sidebar.scss";
import { ProSidebar, Menu, MenuItem, SubMenu } from "react-pro-sidebar";
import {
  Drafts,
  Group,
  Visibility,
  NoteAdd,
  TrendingUp,
  Person,
  Email,
} from "@material-ui/icons";
import { useEffect, useState } from "react";
import useWindowDimensions from "../../../hooks/WindowDimensions";
import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  collapsableWidth,
  imageCollapsableHeight,
} from "../../../constants/Constants";
import baseClasses from "../CSS/Base.module.css";
import HomepageAnimation from "../background/Animation";

const Sidebar = () => {
  const { height, width } = useWindowDimensions();

  const [collapsed, setCollapsed] = useState(false);
  const [image, setShowImage] = useState(false);

  const showCollapsed = () => setCollapsed(true);
  const hideCollapsed = () => setCollapsed(false);

  const showImage = () => setShowImage(true);
  const hideImage = () => setShowImage(false);

  useEffect(() => {
    if (width < collapsableWidth) {
      showCollapsed();
    } else {
      hideCollapsed();
    }

    if (height < imageCollapsableHeight) {
      showImage();
    } else {
      hideImage();
    }
  }, [height, width]);

  return (
    <Col xs={collapsed ? 1 : 2} className={classes.col}>
      <ProSidebar className={classes.sidebar} collapsed={collapsed}>
        <Menu iconShape="square">
          <SubMenu
            title="Instance"
            icon={<Drafts className={baseClasses.icon} />}
            defaultOpen={true}
          >
            <MenuItem
              title="Create"
              icon={<NoteAdd className={baseClasses.icon} />}
            >
              Add
              <Link to="/dashboard/instance/create" />
            </MenuItem>
            <MenuItem icon={<Visibility className={baseClasses.icon} />}>
              View/Edit
              <Link to="/dashboard/instances/view" />
            </MenuItem>
          </SubMenu>
          <MenuItem icon={<Group className={baseClasses.icon} />}>
            Email List <Link to="/dashboard/view_lists" />
          </MenuItem>
          <SubMenu icon={<TrendingUp className={baseClasses.icon} />} title="Statistics"  defaultOpen={true}>
            <MenuItem icon={<Email className={baseClasses.icon}/>}>
              Instance
              <Link to="/dashboard/instances/statistics" />
            </MenuItem>
            <MenuItem icon={<Group className={baseClasses.icon} />}>
              Email List
              <Link to="/dashboard/email_list/statistics" />
            </MenuItem>
            <MenuItem icon={<Person className={baseClasses.icon}/>} >
              Email
              <Link to="/dashboard/email/statistics" />
            </MenuItem>
          </SubMenu>
        </Menu>
        {!collapsed && !image && (
          <HomepageAnimation
            height="auto"
            width="auto"
            bottom="1rem"
            position="fixed"
          />
        )}
      </ProSidebar>
    </Col>
  );
};

export default Sidebar;
