import { useContext } from "react";
import classes from "../topbar/MainHeader.module.css";
import { Navbar, Container, Nav } from "react-bootstrap";
import { ReactComponent as AccountIcon } from "../../resources/icons/account-icon.svg";
import { ReactComponent as HookIcon } from "../../resources/icons/hook-icon.svg";
import { ReactComponent as RegistrationIcon } from "../../resources/icons/registration-icon.svg";
import { ReactComponent as LogoutIcon } from "../../resources/icons/logout-icon.svg";
import { ReactComponent as EmailIcon } from "../../resources/icons/email-icon.svg";
import AuthContext from "../../../context/AuthContext";
import { NavLink } from "react-router-dom";
import baseClasses from "../CSS/Base.module.css"

const Header = (props) => {
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;
  
  const logoutHandler = () => authCtx.logout()

  return (
    <Navbar className={classes.navbar} variant="dark" expand="lg">
      <Container className={classes.container} fluid>
        <Navbar.Brand className={classes.brand} href="/home">
          <h3>
            F<HookIcon className={baseClasses.icon} />
            shi
          </h3>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className={`${classes.nav} ms-auto`}>
          {isLoggedIn && (
              <NavLink className={`${classes.navlink} px-3`} to="/dashboard/instances/statistics">
                <EmailIcon className={baseClasses.icon} /> Dashboard
              </NavLink>
            )}
            {isLoggedIn && (
              <NavLink className={`${classes.navlink} px-3`} to="/profile">
                <AccountIcon className={baseClasses.icon} /> Profile
              </NavLink>
            )}
            {!isLoggedIn ? (
              <NavLink className={`${classes.navlink} px-3`} to="/auth">
                <RegistrationIcon className={baseClasses.icon} /> Register / Login
              </NavLink>
            ) : (
              <NavLink className={`${classes.navlink} px-3`} variant="primary" onClick={logoutHandler} to="/auth">
                <LogoutIcon className={baseClasses.icon} /> Logout
              </NavLink>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
