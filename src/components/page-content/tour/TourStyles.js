export const stepsManual = [
  {
    selector: ".first-step",
    content: "Welcome to Create 📧 window! To begin this tour press arrow ➡️",
  },
   {
    selector: ".first-step",
    content:
      "To close this tour press ❌ or outside of this form!",
  },
  {
    selector: ".second-step",
    content: "Universal tags that you can use in your message. Press here to expand.",
  }, 
  {
    selector: ".third-step",
    content: "Here you must choose provide a URL to a phishing website!",
  },
  {
    selector: ".fourth-step",
    content: "Here you can provide a subject",
  },
  {
    selector: ".fifth-step",
    content:
      "In here you can type your message.",
  }, 
  {
    selector: ".fifth-step",
    content:
      "In every message you must provide a link, you can do this by pressing 🔗 button and adding a title with a link.",
  },
];

export const stepsGenerate = [
  {
    selector: ".first-step",
    content: "Welcome to Generate 📧 window! To begin this tour press arrow ➡️",
  },
  {
    selector: ".first-step",
    content:
      "To close this tour press ❌ or outside of this form!",
  },
  {
    selector: ".second-step",
    content: "Universal tags that you can use in your message. Press here to expand.",
  },  
  {
    selector: ".third-step",
    content: "Here you must choose provide a URL to a phishing website!",
  },
  {
    selector: ".third-step",
    content:
      "In every message you must provide a link, you can do this by pressing 🔗 button and adding a title with a link.",
  },
  {
    selector: ".sixth-step",
    content:
      "By pressing this button you can generate your message text",
  },
];

export const stepsTemplate = [
  {
    selector: ".first-step",
    content: "Welcome to Template window! To begin this tour press arrow ➡️",
  },
  {
    selector: ".first-step",
    content:
      "To close this tour press ❌ or outside of this form!",
  },
  {
    selector: ".first-step",
    content:
      "Here you must choose ✔️ a template! You can do it by selecting category and then choosing a template from dropdown.",
  },
  {
    selector: ".second-step",
    content:
      "Here you must choose provide a URL to a phishing website!",
  },

];
