import {Col, Row} from "react-bootstrap"
import { ReactComponent as BulbIcon } from "../../resources/icons/bulb-icon.svg";
import Popup from "../forms/Popup";

const AdditionalInformationItem = (props) =>{
    return(
        <Row>
            <Col>
               <Popup
                loadData = {false}
                buttonTitle={<BulbIcon style={{fill: "var(--bulbIconColor)"}} />}
                header={"Tag example"}
                classes = {props.btnClass}
                size={props.size}
              >
                <Row>
                <img alt="" src={props.img}></img>
              </Row>
              </Popup>
                <span className={props.classes}>{props.tag}</span>
            </Col>
            <Col>
            <span>{props.desc}</span>
            </Col>
        </Row>
    )
}

export default AdditionalInformationItem