import {Row} from "react-bootstrap"
import AdditionalInformationItem from "./AdditionalInformationItem"
import CustomLink from "../../resources/images/customlink-guide.png"
import Username from "../../resources/images/username-guide.png"
import EmailShort from "../../resources/images/emailshort-guide.png"
import EmailFull from "../../resources/images/emailfull-guide.png"
import Location from "../../resources/images/location-guide.png"
import LocationCountry from "../../resources/images/locationcountry-guide.png"
import IP from "../../resources/images/IP-guide.png"
import Date from "../../resources/images/datenow-guide.png"
import RandomNumber from "../../resources/images/randomnumber-guide.png"

export const AdditionalInformation = (props) => {
  return (
    <Row className={props.classesPara}>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={Username} tag = ":USER-USERNAME:" classes={props.classes} desc="target email"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={EmailShort} tag = ":USER-EMAILSHORT:" classes={props.classes} desc="email without @..."/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={EmailFull} tag = ":USER-EMAILFULL:" classes={props.classes} desc="email with @..."/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={CustomLink} tag = ":CUSTOM-LINK TEXT=YOUR_TEXT:" classes={props.classes} desc="hide your URL in text"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={Location} tag = ":LOCATION-RANDOM:" classes={props.classes} desc="to use random 📍 location"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={LocationCountry} tag = ":LOCATION-RANDOM-COUNTRY:" classes={props.classes} desc="to use random Country 🇦🇼"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={IP} tag = ":IP-RANDOM:" classes={props.classes} desc="to use random IP address"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={Date} tag = ":DATE-NOW:" classes={props.classes} desc="to use date now"/>
      <AdditionalInformationItem size="md" btnClass={props.btnClass} img={RandomNumber} tag = ":NUMBER-RANDOM COUNT=YOUR_NUMBER:" classes={props.classes} desc="to use random Authentification code"/>
    </Row>
  );
};

