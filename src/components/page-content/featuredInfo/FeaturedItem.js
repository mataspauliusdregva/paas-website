import { Col, Row } from "react-bootstrap";
import classes from "./FeaturedItem.module.css";
import { ArrowDownward, ArrowUpward } from "@material-ui/icons";
import baseClasses from "../CSS/Base.module.css";
import { xsWidthData, xsFeaturedItem, xsWidthIcon } from "../../../constants/Constants";
import Lottie from "lottie-react";
import Chart from "../../resources/lottie/chart.json";
const FeaturedItem = (props) => {
  return (
    <Col xs={xsFeaturedItem} className={`${classes.form} ${baseClasses.form}`}>
      <Row className={classes.statistics}>
        <Col xs={xsWidthData}>
          <span className={classes.title}>{props.title}</span>
          <Row className={classes.statistics}>
          <Col>
              <span className={classes.stats}>{props.stats}&nbsp;</span>
              {!props.removeRate &&<span className={classes.rate}>
                {props.rate}
                {props.rate < 0 ? (
                  <ArrowDownward className={classes.arrowDown} />
                ) : (
                  <ArrowUpward className={classes.arrowUp} />
                )}
              </span>}
            </Col>
          </Row>
           <span className={classes.sub}>Compare to last month</span>
        </Col>
        <Col xs={xsWidthIcon}>
          <Lottie
            className={classes.animation}
            animationData={Chart}
            autoPlay={true}
            loop={true}
          />
        </Col>
      </Row>
    </Col>
  );
};

export default FeaturedItem;
