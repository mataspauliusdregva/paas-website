import FeaturedItem from "./FeaturedItem";

const FeaturedInfo = (props) => {

  const toFirstLetterUpperCase = (string) =>{
      const title = string? string.charAt(0).toUpperCase()+ string.slice(1) : ""
      return title
  }

  return (
    <>
      <FeaturedItem stats={props.data.emails} rate={props.data.emails_this_month} title={props.title + " " + toFirstLetterUpperCase(Object.keys(props.data)[0])} />
      <FeaturedItem stats={props.data.links} rate={props.data.links_this_month} title={props.title + " " + toFirstLetterUpperCase(Object.keys(props.data)[2])} />
      <FeaturedItem stats={props.overallData.sent+"/"+props.overallData.total} rate={props.overallData.sent} title={toFirstLetterUpperCase(Object.keys(props.overallData)[0])+"/"+ toFirstLetterUpperCase(Object.keys(props.overallData)[1])} removeRate={true}/>
    </>
  );
};

export default FeaturedInfo;
