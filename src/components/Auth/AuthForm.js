import { useState, useRef, useContext } from "react";
import { Form, Button, Row, Container, Col } from "react-bootstrap";
import classes from "../page-content/CSS/Base.module.css";
import { LinearProgress } from "@material-ui/core";
import AuthContext from "../../context/AuthContext";
import { signUpUrl, signInUrl } from "../../constants/Urls";
import { useHistory } from "react-router-dom";
import { FormControlLabel, Checkbox } from "@mui/material";
import { fetchData } from "../../utils/FetchData";
import { checkboxStyles } from "./AuthFormStyles";
import SwitchButton from "../page-content/buttons/SwitchButton";
import { containerWidth, CREATEACCOUNT, CREATENEWACCOUNT, LOGINWITHEXISTING, SIGNUPUSER, TERMSANDCONDITIONS, xsWidthUserForm } from "../../constants/Constants";
import { LOGINUSER } from "../../constants/Constants";
import FormGroup from "../page-content/forms/FormGroup";

const AuthForm = () => {

  const history = useHistory();
  const authCtx = useContext(AuthContext);

  const nameInputRef = useRef();
  const surnameInputRef = useRef();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const usernameInputRef = useRef();
  
  const [isLogin, setIsLogin] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [checkbox, setCheckbox] = useState(false)

  const switchAuthModeHandler = () => setIsLogin((prevState) => !prevState);
  
  const submitHandler = (event) => {
    event.preventDefault();

    const enteredUsername = usernameInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    const success = (data) => {
      setIsLoading(false);
      authCtx.login(data);
      history.push("/home");
    };

    const failure = (err) => {
      setIsLoading(false);
      alert(err.message);
    };

    setIsLoading(true);
    let url;
    let body;
    if (isLogin) {
      url = signInUrl;
      body = JSON.stringify({
        username: enteredUsername,
        password: enteredPassword,
      });
    } else {

      const enteredEmailAdress = emailInputRef.current.value;
      const firstName = nameInputRef.current.value;
      const lastName = surnameInputRef.current.value;

      url = signUpUrl;
      body = JSON.stringify({
        username: enteredUsername,
        password: enteredPassword,
        email: enteredEmailAdress,
        first_name: firstName,
        last_name: lastName,
      });
    }

    fetchData(
      url,
      {
        "Content-Type": "application/json",
      },
      "POST",
      success,
      failure,
      body
    );
  };

  return (
    <Container className={containerWidth}>
    <Col xs={xsWidthUserForm}>
    <Form className={classes.form} onSubmit={submitHandler}>
      <h1 className={classes.h1}>{isLogin ? LOGINUSER : SIGNUPUSER}</h1>
      <Row>
        {!isLogin && (
          <>
            <FormGroup label="Name" type="text" reference={nameInputRef} maxlen = "150"/>
            <FormGroup label="Surname" type="text" reference={surnameInputRef} maxlen = "150"/>
            <FormGroup label="Email Address" type="email" reference={emailInputRef} maxlen = "255"> 
            <Form.Text className={classes.input}>
                We'll never share your email with anyone else.
            </Form.Text>
            </FormGroup>
          </>
        )}
        <FormGroup label="Username" type="text" reference={usernameInputRef} maxlen="150"/>
        <FormGroup label="Password" type="password" reference={passwordInputRef} maxlen="128"/>
        {!isLogin && <FormGroup label="Confirm Password" type="password" maxlen="128"/>}
        {!isLoading ? (
          <Button className={classes.button} variant="primary" type="submit">
            {isLogin ? LOGINUSER : CREATEACCOUNT}
          </Button>
        ) : (
          <LinearProgress className={classes.loading} />
        )}
        {!isLogin && (
          <FormControlLabel
            control={
              <Checkbox
                defaultChecked={false}
                sx={checkboxStyles}
                required
              />
            }
            label={TERMSANDCONDITIONS}
          />
        )}
        <SwitchButton classes={classes.actions} trueValue={CREATENEWACCOUNT} falseValue={LOGINWITHEXISTING} state={isLogin} switchModeHandler={switchAuthModeHandler}/>
      </Row>
    </Form>
    </Col>
    </Container>
  );
};

export default AuthForm;