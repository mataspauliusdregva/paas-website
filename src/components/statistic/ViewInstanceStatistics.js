import Statistics from "../page-content/forms/statistics/Statistics";
import { getInstances, getInstanceStatisticsSent, getInstanceStatisticsOpened, getInstanceStatisticsFromInterval, getInstanceStatisticsList} from "../../constants/Urls";
import { emailStatisticsColumns } from "../page-content/table/Columns";

const ViewInstanceStatistics = (props) => {
  return (<Statistics from={props.from} getData={getInstances} getSentData={getInstanceStatisticsSent} getOpenedData={getInstanceStatisticsOpened} getMonthsData={getInstanceStatisticsFromInterval} tableUrl={getInstanceStatisticsList} columns={emailStatisticsColumns} name="instance"/>);
};

export default ViewInstanceStatistics;
