import { getListsStatisticsOpened, getListsStatisticsSent, getListsStatisticsTable, getListStatisticsMonths, getUserEmailLists } from "../../constants/Urls";
import Statistics from "../page-content/forms/statistics/Statistics";
import { instanceStatisticsColumns } from "../page-content/table/Columns";

const ViewEmailListStatistics = (props) => {
    return (<Statistics from={props.from} getData={getUserEmailLists} getSentData={getListsStatisticsSent} getOpenedData={getListsStatisticsOpened} getMonthsData={getListStatisticsMonths} tableUrl={getListsStatisticsTable} columns={instanceStatisticsColumns} name="list"/>);
  };
  
  export default ViewEmailListStatistics;