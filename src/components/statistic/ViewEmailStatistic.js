import { getAllEmails, getEmailStatisticsMonth, getEmailStatisticsOpened, getEmailStatisticsSent, getEmailStatisticsTable } from "../../constants/Urls";
import Statistics from "../page-content/forms/statistics/Statistics";
import { instanceStatisticsColumns } from "../page-content/table/Columns";

const ViewEmailStatistics = (props) => {
    return (<Statistics from={props.from} getData={getAllEmails} getSentData={getEmailStatisticsSent} getOpenedData={getEmailStatisticsOpened} getMonthsData={getEmailStatisticsMonth} tableUrl={getEmailStatisticsTable} columns={instanceStatisticsColumns} name="email"/>);
  };
  
  export default ViewEmailStatistics;