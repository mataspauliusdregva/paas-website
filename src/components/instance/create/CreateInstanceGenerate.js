import CreateInstance from "../../page-content/forms/instance/CreateInstance";

const CreateInstanceGenerate = (props) => {
  const tourSteps = ["first-step", "second-step", "third-step", "fourth-step", "fifth-step", "sixth-step"];
 return(
   <CreateInstance tourSteps={tourSteps} type="Generate" switchState={props.switchState} generatedMessage={props.generatedMessage} title={props.title} header="Generate Text" choice={props.choice} url= {props.url}/>
  );
};

export default CreateInstanceGenerate;