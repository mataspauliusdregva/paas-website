import CreateInstance from "../../page-content/forms/instance/CreateInstance";

const CreateInstanceTemplate = (props) =>{
    const tourSteps = ["first-step", "second-step"];
    
   return(<CreateInstance tourSteps={tourSteps} type="Template" switchState={props.switchState} template={props.template} header="Choose Template" link={props.link} choice={props.choice}  url= {props.url} title={props.title}/>)
}

export default CreateInstanceTemplate