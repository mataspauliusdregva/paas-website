import CreateInstance from "../../page-content/forms/instance/CreateInstance";

const CreateInstanceManually = (props) => {
  const tourSteps = ["first-step", "second-step", "third-step", "fourth-step", "fifth-step"];
 return(
   <CreateInstance tourSteps={tourSteps} type="Manual" switchState={props.switchState} message={props.message} title={props.title} header="Create Text" choice={props.choice}  url= {props.url}/>
  );
};

export default CreateInstanceManually;
