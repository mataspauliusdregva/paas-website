import Instance from "../../page-content/forms/instance/Instance"

const CreateInstanceMain = (props) =>{
    
    return(
        <Instance type="create" header = "Create Instance" state={props.state} enable={props.enable} switchState={props.switchState} generatedMessage = {props.generatedMessage} message={props.message} template={props.template} messageTitle="Preview:" link={props.link} title={props.title} choice={props.type} resetChoice={props.resetType} url={props.url}/>
    )
}

export default CreateInstanceMain