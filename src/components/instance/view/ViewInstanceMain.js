import Instance from "../../page-content/forms/instance/Instance"

const ViewInstanceMain = (props) =>{
    return(
        <Instance type="update" header = "Update Instance" state={true} enable={props.enable} switchState={props.switchState} message={props.message} messageTitle="Your message:"/>
    )
}

export default ViewInstanceMain