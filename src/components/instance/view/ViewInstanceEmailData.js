import { useContext, useState, useCallback, useEffect } from "react";
import AuthContext from "../../../context/AuthContext";
import { fetchData } from "../../../utils/FetchData";

import Table from "../../page-content/table/Table";
import { xsWidthChart } from "../../../constants/Constants";
import InstanceContext from "../../../context/InstanceContext";

const ViewTableData = (props) => {
  const { authTokens } = useContext(AuthContext);
  const {emailListState} = useContext(InstanceContext)
  const [listRows, setListRows] = useState([]);

  const failure = (err) => console.log(err.message);

  const loadLists = useCallback(() => {
    const success = (data) => {
      console.log(data)
      setListRows(data);
    };

    fetchData(
      props.tableUrl,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access, props.tableUrl]);

  useEffect(() => {
    loadLists();
  }, [loadLists, emailListState]);

  return (
    <Table
      xs={xsWidthChart}
      columns={props.columns}
      data={listRows}
      height={props.height}
      type="instance"
      actions={true}
      flex={3}
      removePadding="px-0"
      refreshButton={true}
      expandButton = {true}
    />
  );
};

export default ViewTableData;
