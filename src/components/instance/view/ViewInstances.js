import List from "../../page-content/table/Table";
import { xsWidthTableFull } from "../../../constants/Constants";
import { useContext, useState } from "react";
import { instanceColumns } from "../../page-content/table/Columns";
import { useCallback, useEffect } from "react";
import AuthContext from "../../../context/AuthContext";
import { fetchData } from "../../../utils/FetchData";
import { deleteInstance, deleteInstnaceMultiple, getInstances, sendEmail } from "../../../constants/Urls";
import InstanceContext from "../../../context/InstanceContext";

const ViewInstances = () => {
  const [instances, setInstances] = useState([]);

  const { authTokens, state } = useContext(AuthContext);
  const {loadState} = useContext(InstanceContext)

  const failure = (err) => alert(err.message);
  const success = (data) => setInstances(data);

  const loadInstances = useCallback(() => {
    fetchData(
      getInstances,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access]);


  useEffect(() => {
     loadInstances();
  }, [loadInstances, state, loadState]);

  return (
    <List
      xs={xsWidthTableFull}
      columns={instanceColumns}
      data={instances}
      height={"calc(100vh - 135px)"}
      type="instance"
      deleteButton={true}
      addButton={false}
      editButton={true}
      descriptionButton = {true}
      checkBoxButton={true}
      sendUrl = {sendEmail}
      deleteUrl={deleteInstance}
      deleteMultipleUrl={deleteInstnaceMultiple}
      flex = {3}
    />
  );
};

export default ViewInstances;
