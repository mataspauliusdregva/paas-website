import { useState, useEffect, useCallback, useContext, useRef } from "react";
import { getInstance, updateInstance } from "../../../constants/Urls";
import AuthContext from "../../../context/AuthContext";
import { fetchData } from "../../../utils/FetchData";
import { Col, Form, Button } from "react-bootstrap";
import baseClasses from "../../page-content/CSS/Base.module.css"
import { universalStyles } from "../../../constants/Styles";
import EmailListContext from "../../../context/EmailListContext";
import { useParams } from "react-router-dom";
import TextField from "../../page-content/forms/TextField";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import classes from "../../page-content/forms/statistics/Statistics.module.css"
import {ContentState } from "draft-js";
import htmlToDraft from "html-to-draftjs";
import draftToHtml from "draftjs-to-html";
import { convertToRaw } from "draft-js";
import { validateData, validateLink } from "../../page-content/forms/Validation";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import CreateInstance from "../../page-content/forms/instance/CreateInstance";
import { Autocomplete, TextField as Textfield } from "@mui/material";

const EditInstance = () => {
  const { authTokens, updateState } = useContext(AuthContext);

  const [instance, setInstance] = useState([]);
  const [error, setError] = useState("");
  const [linkError, setLinkError] = useState("");

  const [defaultSelectValue, setDefaultSelectValue] = useState("")
  const emailListCtx = useContext(EmailListContext)

  const nameInputRef = useRef()
  const descriptionInputRef = useRef()
  const urlInputRef = useRef()
  const titleInputRef = useRef()

  const params = useParams()
  const history = useHistory()

  const url = getInstance + params.instanceId

  const [editorstate, setEditorState] = useState(EditorState.createEmpty());
  const onEditorStateChange = (editorState) => setEditorState(editorState);

  const failure = (err) => alert(err.message);

  const loadInstance = useCallback(() => {
    const success = (data) => {
      console.log(data)
      setInstance(data)
      setDefaultSelectValue(emailListCtx.list.filter((item)=> item.id === data.email_list)[0])
      nameInputRef.current.value = data.name
      descriptionInputRef.current.value = data.description
      urlInputRef.current.value = data.url
      titleInputRef.current.value = data.email_title
      const contentBlock = htmlToDraft(data.email_text)
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      setEditorState(editorState)
    };
    fetchData(
      url,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure,
    );
    
  }, [authTokens.access, url, emailListCtx.list]);

  useEffect(() => {
    loadInstance();
  }, [loadInstance]);

  const handleChange = (event) =>{
    const list = emailListCtx.list.filter((item) => item.listName === event.target.value)
    setDefaultSelectValue(list[0])
  }


  const successUpddate = () => {
    history.push("/dashboard/instances/view") 
    updateState()};
 
  const handleSubmit = (event) =>{
    event.preventDefault()

    if(validateLink(urlInputRef.current.value, setLinkError) && validateData(titleInputRef.current.value, "Subject is empty!", setError)){
      fetchData(
        updateInstance,
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + String(authTokens.access),
        },
        "PUT",
        successUpddate,
        failure,
        JSON.stringify({
          id: instance.id,
          name: nameInputRef.current.value,
          list_id: defaultSelectValue.id,
          title: titleInputRef.current.value,
          text: draftToHtml(convertToRaw(editorstate.getCurrentContent())),
          description: descriptionInputRef.current.value,
          url: urlInputRef.current.value
        })
      );
    }
  }

  return (
    <>
    <Col>
     <Form className={`${baseClasses.form} ${classes.form}`}>
     <h1 className={classes.h1}>Update Instance</h1>
     <TextField label="Name" header="Instance name" classes={classes} inputRef={nameInputRef} maxlen="255"/>
      <p className={classes.para}>Select Email List</p>
      <Autocomplete
              className={classes.control}
              id="instances"   
              autoSelect={true}
              options={emailListCtx.list}
              value = {defaultSelectValue || ''}
              isOptionEqualToValue={(option, value) => option.id === value.id}
              getOptionLabel={option => option.listName || ''}
              onSelect={handleChange}
              renderInput={(params) => <Textfield sx={universalStyles} {...params} label="List"/>}
            />
          <TextField label="Description" header="Description" classes={classes} inputRef={descriptionInputRef}/>
          <Button
              className={`${baseClasses.button} ${classes.button}`}
              variant="primary"
              type="submit"
              onClick={handleSubmit}
            >
              Update
            </Button>
      </Form>
    </Col>
      <CreateInstance type="Manual" update={true}  header="Update Instance" subjectInputRef={titleInputRef} urlInputRef={urlInputRef} editorState={editorstate} linkError={linkError} error={error} setError={setError} setLinkError={setLinkError} editorStateChange={onEditorStateChange}/>
    </>
  );
};

export default EditInstance;
