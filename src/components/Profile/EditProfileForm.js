import { Form, Button, Container, Col } from "react-bootstrap";
import { useRef, useState, useContext, useEffect, useCallback } from "react";
import classes from "../page-content/CSS/Base.module.css";
import editProfileClasses from "./EditProfileForm.module.css";
import { changeAccountDataUrl, getUserData } from "../../constants/Urls";
import { LinearProgress } from "@material-ui/core";
import Layout from "../layout/Layout";
import AuthContext from "../../context/AuthContext";
import { useHistory } from "react-router-dom";
import { fetchData } from "../../utils/FetchData";
import { containerWidth, xsWidthUserForm } from "../../constants/Constants";
import FormGroup from "../page-content/forms/FormGroup";

const ProfileForm = () => {
  const history = useHistory();

  const newPasswordInputRef = useRef();
  const newConfirmPasswordInputRef = useRef();
  const newEmailInputRef = useRef();
  const newNameInputRef = useRef();
  const newSurnameInputRef = useRef();
  const newUsernameInputRef = useRef();

  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState([]);

  const { authTokens } = useContext(AuthContext);

  const failure = (err) => {
    setIsLoading(false);
    alert(err.message);
  };

  const getUser = useCallback(() => {
    const success = (data) => {
      setUser(data);
    };
    fetchData(
      getUserData,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access]);

  useEffect(() => {
    getUser();
  }, [getUser]);

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredPassword = newPasswordInputRef.current.value;
    const enteredConfirmPassword = newConfirmPasswordInputRef.current.value;
    const enteredEmail = newEmailInputRef.current.value;
    const enteredUsername = newUsernameInputRef.current.value;
    const enteredName = newNameInputRef.current.value;
    const enteredSurname = newSurnameInputRef.current.value;

    setIsLoading(true);
    const success = () => {
      setIsLoading(false);
      newPasswordInputRef.current.value = "";
      newConfirmPasswordInputRef.current.value = "";
      history.replace("/profile");
    };

    const body = JSON.stringify({
      username: enteredUsername,
      password: enteredPassword,
      confirm_password: enteredConfirmPassword,
      email: enteredEmail,
      first_name: enteredName,
      last_name: enteredSurname,
    });

    fetchData(
      changeAccountDataUrl,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "PUT",
      success,
      failure,
      body
    );
  };

  return (
    <Container className={containerWidth}>
      <Col xs={xsWidthUserForm}>
        <Form className={classes.form} onSubmit={submitHandler}>
          <Layout>
            <h1 className={classes.h1}>Account Information</h1>
            <FormGroup
              label="Name"
              type="text"
              defaultValue={user.first_name}
              reference={newNameInputRef}
              maxlen = "150"
            />
            <FormGroup
              label="Surname"
              type="text"
              defaultValue={user.last_name}
              reference={newSurnameInputRef}
              maxlen = "150"
            />
            <FormGroup
              label="Email"
              type="email"
              defaultValue={user.email}
              reference={newEmailInputRef}
              maxlen = "255"
            />
            <FormGroup
              label="Username"
              type="text"
              defaultValue={user.username}
              reference={newUsernameInputRef}
              maxlen = "150"
            />
            <FormGroup
              label="New Password"
              type="password"
              reference={newPasswordInputRef}
              maxlen = "128"
            />
            <FormGroup
              label="Confirm New Password"
              type="password"
              reference={newConfirmPasswordInputRef}
              maxlen = "128"
            />
            {!isLoading ? (
              <Button
                className={`${classes.button} ${editProfileClasses.button}`}
                variant="primary"
                type="submit"
              >
                Submit
              </Button>
            ) : (
              <LinearProgress className={classes.loading} />
            )}
          </Layout>
        </Form>
      </Col>
    </Container>
  );
};

export default ProfileForm;
