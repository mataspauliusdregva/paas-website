export const stylesForTextField = {
  "& .MuiInputLabel-filled": {
    color: "black",
  },
  "& label.Mui-focused": {
    color: "black",
  },
  "& .css-cio0x1-MuiInputBase-root-MuiFilledInput-root:after": {
    color: "black",
  },
  "& .css-cio0x1-MuiInputBase-root-MuiFilledInput-root:before": {
    borderBottomColor: "black",
  },
  underline: {
    "&:before": {
      borderBottomColor: "white",
    },
    "&:after": {
      borderBottomColor: `white`,
    },
    "&:hover:not($disabled):not($focused):not($error):before": {
      borderBottomColor: `white`,
    },
    "&": {
      borderBottomColor: `white`,
    },
  },

  "& .MuiFilledInput-root:after": {
    borderBottomColor: `white`,
  },

  input: { color: "black", backgroundColor: "white", underline: `white` },
};

export const universalStyles = {
  "& .MuiOutlinedInput-notchedOutline": {
    borderColor: "white",
  },
  "& .Mui-focused .MuiOutlinedInput-notchedOutline": {
    borderColor: "white",
  },
  "& :hover .MuiOutlinedInput-notchedOutline": {
    borderColor: "white",
  },
  "& .Mui-disabled": {
    color: "white",
  },
  "& .css-14s5rfu-MuiFormLabel-root-MuiInputLabel-root": {
    color: "white",
  },
  "& .css-1sumxir-MuiFormLabel-root-MuiInputLabel-root": {
    color: "white",
  },
  "& .MuiOutlinedInput-root": {
    color: "white",
    borderColor: "white",
    "&.Mui-focused fieldset": {
      borderColor: "white",
    },
    "&:hover fieldset": {
      borderColor: "white",
    },
    "& .MuiInput-underline": {
      borderBottomColor: "white",
    },
  },

  "& .MuiInputBase-root.Mui-disabled": {
    color: "white !important",
  },
  "& .MuiFormLabel-root.Mui-disabled": {
    color: "white !important",
  },

  "& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input.Mui-disabled": {
    WebkitTextFillColor: "white !important",
  },

  "& .css-hfutr2-MuiSvgIcon-root-MuiSelect-icon": {
    color: "white",
    borderColor: "white",
  },
  "& .css-bpeome-MuiSvgIcon-root-MuiSelect-icon": {
    color: "white",
    borderColor: "white",
  },

  "& .css-1sumxir-MuiFormLabel-root-MuiInputLabel-root.Mui-focused": {
    color: "white!important",
    borderColor: "white!important",
  },

  "& label.Mui-focused": {
    color: "white",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "yellow",
  },

  "& .css-9ddj71-MuiInputBase-root-MuiOutlinedInput-root.Mui-disabled .MuiOutlinedInput-notchedOutline":
    {
      borderColor: "white !important",
      color: "white !important",
      fill: "white !important",
    },
    "& .MuiSvgIcon-root":{
      fill: "white !important",
    }
};

