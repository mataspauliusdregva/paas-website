
export const checkType = (data) =>{
    return data === "update" ? false : true;
}

export const createExampleTemplate = (template, emailFullRegex, usernameRegex, emailShortRegex, locationRandRegex, locationRandCountryRegex, ipRandRegex, dateNowRegex, randNumberRegex, urlRegex, textInUrlRegex) =>{

    const date = new Date()
    const dateNow = "<br/>"+date.toDateString() +" "+ date.toLocaleTimeString();
    const text = template.match(textInUrlRegex)

    let data =  template.replace(emailFullRegex, "john.doe@email.com")
    .replace(emailShortRegex, "john.doe")
    .replace(usernameRegex, "John Doe")
    .replace(locationRandCountryRegex, "France")
    .replace(locationRandRegex, "Paris France")
    .replace(ipRandRegex, "85.168.45.15")
    .replace(dateNowRegex, dateNow)
    .replace(randNumberRegex, "458 917")

    if(text !==null && text.length !==0){
        for(let i=0; i<text.length; i++)
        {   
            data = data.replace(urlRegex, "<a href=''>"+text[i]+"</a>")
        }
    }

   return data
}
