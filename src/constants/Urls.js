const host = "http://78.31.188.217:8080"; //"http://127.0.0.1:8000"; //http://78.31.188.217:8080

//auth
export const signUpUrl = host + "/api/user/create";
export const signInUrl = host + "/api/token/";
export const changeAccountDataUrl = host + "/api/user/update";
export const refreshToken = host + "/api/token/refresh/";

//create
export const createList = host + "/api/list/create";
export const createEmail = host + "/api/list/emails/add";
export const createMultipleEmail = host + "/api/list/emails/add/multiple";
export const createInstanceManual = host + "/api/instance/email/create/manual";
export const createInstanceGenerate =
  host + "/api/instance/email/create/generated";
export const createInstanceTemplate =
  host + "/api/instance/email/create/template";
export const createInstance = host + "/api/instance/create";
export const sendEmail = host + "/api/instance/start";
export const sendEmailWithoutSaving = host + "/api/instance/commit";
export const instanceGenerate = host + "/api/instance/article/generate"

//get
export const getAllTags = host + "/api/instance/article/tags"
export const getAllEmails = host + "/api/list/emails/all"
export const getUserData = host + "/api/user/";
export const getUserEmailLists = host + "/api/list/";
export const getEmails = host + "/api/list/emails?list_id=";
export const getEmailData = host + "/api/list/emails/get?list_id=";
export const getListFormData = host + "/api/list/emails/forms";
export const getFormCategories = host + "/api/instance/categories";
export const getInstances = host + "/api/instance/";
export const getInstance = host + "/api/instance/get?instance=";
export const getInstanceStatuses = host + "/api/instance/status";
export const getInstanceStatisticsSent =
  host + "/api/statistic/instance/sent?id=";
export const getInstanceStatisticsOpened =
  host + "/api/statistic/instance/opened?id=";
export const getInstanceStatisticsList =
  host + "/api/statistic/instance/table?id=";
export const getInstanceStatisticsFromInterval =
  host + "/api/statistic/instance/months?id=";

export const getEmailStatisticsSent =
  host + "/api/statistic/email/sent?id=";
export const getEmailStatisticsOpened =
  host + "/api/statistic/email/opened?id=";
export const getEmailStatisticsTable =
  host + "/api/statistic/email/table?id=";
export const getEmailStatisticsMonth =
  host + "/api/statistic/email/months?id=";


export const getListsStatisticsSent =
  host + "/api/statistic/email_list/sent?id=";
export const getListsStatisticsOpened =
  host + "/api/statistic/email_list/opened?id=";
export const getListsStatisticsTable =
  host + "/api/statistic/email_list/table?id=";
export const getListStatisticsMonths =
  host + "/api/statistic/email_list/months?id=";

//delete
export const deleteList = host + "/api/list/remove";
export const deleteEmail = host + "/api/list/emails/remove";
export const deleteListMultiple = host + "/api/list/remove/multiple";
export const deleteEmailMultiple = host + "/api/list/emails/remove/multiple";
export const deleteInstance = host + "/api/instance/remove";
export const deleteInstnaceMultiple = host + "/api/instance/remove/multiple";

//update
export const updateEmailData = host + "/api/list/emails/update";
export const updateInstance = host + "/api/instance/update";
