//colors
export const instanceStepColor = "#ffa345"
export const templateGroupColors = ["#1e53b9", "#1e53b9", "#1e53b9", "#1e53b9", " #1e53b9", "#1e53b9"]

//table
export const xsWidthTableFull = 10
export const xsWidthTable = 6 
export const xsWidthCreateFillUserData =  10 - xsWidthTable
export const xsWidthCreateLists = 10 - xsWidthTable

export const statisticsDefaultSize = "calc(62vh - 135px)"
export const statisticsLowerSize = "calc(45vh - 135px)"
export const statisticsBiggerSize = "calc(85vh - 135px)"
export const statisticsBiggestSize = "calc(100vh - 135px)"
//instance
export const xsWidthInstance = 4
export const xsWidthCreateInstance = 6
export const xsWidthInstanceFormRowCheckbox = 2
export const xsWidthInstanceFormRowNumber = 2
export const xsWidthInstanceFormRowInput = 8
export const xsTemplateButton = 4
export const xsWidthInstanceInfo = 9

//user
export const xsWidthUserForm = 4
export const xsWidthUserFormTable = 12

//rows
export const rowWidthGenerateInstanceForm = "auto"
export const containerWidth = "d-flex justify-content-center"

//auth
export const TERMSANDCONDITIONS = "Agree to Terms and Conditions"
export const LOGINWITHEXISTING = "Login with existing account"
export const LOGINUSER = "Login"
export const SIGNUPUSER = "Sign Up"
export const CREATEACCOUNT="Create Account"
export const CREATENEWACCOUNT = "Create new account"

//dim
export const collapsableWidth = 1100
export const imageCollapsableHeight = 665

//regex
export const usernameRegex = /(:USER-)[A-Z]*:/g;
export const emailFullRegex = /(:USER-EMAILFULL):/g;
export const emailShortRegex = /(:USER-EMAILSHORT):/g;
export const locationRandRegex = /(:LOCATION-RANDOM):/g;
export const locationRandCountryRegex = /(:LOCATION-RANDOM-COUNTRY):/g;
export const ipRandRegex = /(:IP-RANDOM):/g;
export const dateNowRegex = /(:DATE-NOW):/g;
export const randNumberRegex = /(:NUMBER-RANDOM COUNT=.*(?=\:)):/g;
export const textInUrlRegex = /(?<=TEXT=)(.*?)(?=\:)/g;
export const urlRegex = /(:CUSTOM-LINK TEXT=.*(?=\:):)/
//chart
export const aspectRatio = (9/1)
export const xsWidthChart = 11

//information
export const xsWidthIcon = 3
export const xsWidthData = 9
export const xsFeaturedItem = 3


//statuses
export const finishedStatus = 3
export const inProgress = 4
export const idle = 1

//ids
export const newsCategoryId = 7
export const plainTypeId = 20