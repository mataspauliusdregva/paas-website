import { getFormCategories, getListFormData } from "./Urls";
import { fetchData } from "../utils/FetchData";

export const listFormData = new Promise((resolve,reject) => {

  const failure = (err) => reject(err)
  const success = (data) =>resolve(data);
 
  fetchData(
   getListFormData,
    {
      "Content-Type": "application/json",
    },
    "GET",
    success,
    failure,
  );
});

export const formCategories = new Promise((resolve,reject) => {

  const failure = (err) => reject(err)
  const success = (data) => resolve(data);

  fetchData(
   getFormCategories,
    {
      "Content-Type": "application/json",
    },
    "GET",
    success,
    failure,
  );
});

