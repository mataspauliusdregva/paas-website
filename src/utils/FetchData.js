const handleBody = (body, url, method, header) => {
  if (!body) {
    return fetch(url, { method: method, headers: header });
  } else {
    return fetch(url, { method: method, headers: header, body: body });
  }
};

export const fetchData = (
  url,
  header,
  method,
  successFunction,
  failureFunction,
  body
) => {
  handleBody(body, url, method, header)
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        return res.json().then((data) => {
          let errorMessage = data.response;
          throw new Error(errorMessage);
        });
      }
    })
    .then((data) => successFunction(data))
    .catch((err) => failureFunction(err));
};
