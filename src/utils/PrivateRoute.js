import { Route, Redirect } from "react-router-dom";
import AuthContext from "../context/AuthContext";
import { useContext } from "react";
import React from 'react';

const PrivateRoute = ({children, ...rest}) => {
  const authCtx = useContext(AuthContext);
  return (
    <Route {...rest}>
      {!authCtx.isLoggedIn ? <Redirect to="/auth"></Redirect> : children}
    </Route>
  );
};

export default PrivateRoute;
