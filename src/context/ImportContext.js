import React from "react";
import { useState } from "react";

const ImportContext = React.createContext();

export const ImportContextProvider = (props) => {

  const [importState, setImportState] = useState(false)

  const importHandler = (bool) => {
    setImportState(bool);
  };

  const contextValue = {
    state: importState,
    setState: importHandler 
  };

  return (
    <ImportContext.Provider value={contextValue}>
      {props.children}
    </ImportContext.Provider>
  );
};

export default ImportContext;