import React, { useState, useEffect, useCallback } from "react";
import { refreshToken } from "../constants/Urls";
import { fetchData } from "../utils/FetchData";

const AuthContext = React.createContext();

export const AuthContextProvider = (props) => {
  
  let [authTokens, setTokens] = useState(() =>
    localStorage.getItem("authTokens")
      ? JSON.parse(localStorage.getItem("authTokens"))
      : null
  );
 
  let [loading, setLoading] = useState(true);
  
  const [state, setState] = useState(false)
  const userIsLoggedIn = authTokens ? true : false;

  let stateHandler = () =>setState((prevState) => !prevState);
  
  let loginHandler = (data) => {
    setTokens(data);
    localStorage.setItem("authTokens", JSON.stringify(data));
  };

  let logoutHandler = () => {
    setTokens(null);
    localStorage.removeItem("authTokens");
  };

  let updateToken = useCallback(() => {

    const success = (data) => {
      setTokens(data);
      localStorage.setItem("authTokens", JSON.stringify(data));
    }
  
    const failure = (err) =>logoutHandler()

    const body = JSON.stringify({ refresh: authTokens?.refresh })
  
    fetchData(refreshToken, {
      "Content-Type": "application/json",
    }, "POST", success, failure, body)

    if (loading) {
      setLoading(false);
    }
 
  }, [authTokens?.refresh, loading]);

  const contextValue = {
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
    authTokens: authTokens,
    state: state,
    updateState: stateHandler
  };
  
  useEffect(() => {
    if (loading) {
      updateToken();
    }
    let intervalTime = 1000 * 60 * 4;
    let interval = setInterval(() => {
      if (authTokens) {
        updateToken();
      }
    }, intervalTime);
    return () => {
      clearInterval(interval);
    };
  }, [authTokens, loading, updateToken]);
  
  return (
    <AuthContext.Provider value={contextValue}>
      {loading ? null : props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;