import React, { useState, useCallback, useContext, useEffect } from "react";
import { getUserEmailLists } from "../constants/Urls";
import { fetchData } from "../utils/FetchData";
import AuthContext from "./AuthContext";

const EmailListContext = React.createContext();

export const EmailListContextProvider = (props) => {
  const {authTokens, state} = useContext(AuthContext)
  const [emailList, setEmailList] = useState([]);

  const failure = (err) => alert(err.message);

  const loadLists = useCallback(() => {
    const success = (data) => {
      const loadedLists = [];
      for (const key in data) {
        if (!isNaN(+key)) {
            loadedLists.push({
              id: data[key].id,
              listName: data[key].name,
              dateCreated: data[key].created_at,
              amount: data[key].email_count,
              used_in: data[key].used_in
            });
        }
      }
      setEmailList(loadedLists);
    };

    fetchData(
      getUserEmailLists,
      {
        "Content-Type": "application/json",
        Authorization: "Bearer " + String(authTokens.access),
      },
      "GET",
      success,
      failure
    );
  }, [authTokens.access]);

  useEffect(() => {
    loadLists();
  }, [loadLists, state]);

  const updateListHandler = (data) => {
    setEmailList(data);
  };

  const contextValue = {
    list: emailList,
    update: updateListHandler,
    fetchList: loadLists,
  };
  return (
    <EmailListContext.Provider value={contextValue}>
      {props.children}
    </EmailListContext.Provider>
  );
};

export default EmailListContext;
