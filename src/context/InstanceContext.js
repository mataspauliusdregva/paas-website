import React from "react";
import { useState } from "react";

const InstanceContext = React.createContext();

export const InstanceContextProvider = (props) => {

  const [instanceLoadState, setInstanceLoadState] = useState(false)
  const [instanceEmailListState, setInstanceEmailListState] = useState(false)
  const [expandTable, setExpandTable] = useState(false)

  const handleLoadState = () => {
    setInstanceLoadState((prevState) => !prevState);
  };

  const handleEmailListState = () =>{
      setInstanceEmailListState((prevState) => !prevState)
  }

  const handleExpandTable = () =>{
    setExpandTable((prevState) => !prevState)
  }

  const contextValue = {
    loadState: instanceLoadState,
    emailListState: instanceEmailListState,
    setState: handleLoadState,
    setEmailListState: handleEmailListState,
    expand: expandTable,
    setExpand: handleExpandTable

  };

  return (
    <InstanceContext.Provider value={contextValue}>
      {props.children}
    </InstanceContext.Provider>
  );
};

export default InstanceContext;