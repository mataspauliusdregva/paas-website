import React from "react";
import { useState } from "react";

const GuideContext = React.createContext();

export const GuideContextProvider = (props) => {

  const [guideState, setGuideState] = useState(true)

  const guideHandler = (bool) => {
    setGuideState(bool);
  };

  const contextValue = {
    guideState: guideState,
    guideSetState: guideHandler 
  };
  return (
    <GuideContext.Provider value={contextValue}>
      {props.children}
    </GuideContext.Provider>
  );
};

export default GuideContext;