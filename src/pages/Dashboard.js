import Background from "../components/page-content/background/Background";
import { RemoveScrollBar } from "react-remove-scroll-bar";
import { Row } from "react-bootstrap";
import Sidebar from "../components/page-content/sidebar/Sidebar";
import ViewEmailLists from "../components/email/view/ViewEmailLists";
import ViewEmails from "../components/email/view/ViewEmails";
import ViewUser from "../components/email/view/ViewEmail";
import PrivateRoute from "../utils/PrivateRoute";
import { Switch } from "react-router-dom";
import CreateEmailList from "../components/email/create/CreateEmailList";
import CreateEmail from "../components/email/create/CreateEmail";
import "../components/page-content/tour/Tour.module.css";
import InstancePage from "./InstancePage";
import Typing from "../components/resources/video/typing.mp4";
import ViewInstances from "../components/instance/view/ViewInstances";
import { EmailListContextProvider } from "../context/EmailListContext";
import ViewInstanceStatistics from "../components/statistic/ViewInstanceStatistics";
import { ImportContextProvider } from "../context/ImportContext";
import { GuideContextProvider } from "../context/GuideContext";
import EditInstance from "../components/instance/view/UpdateInstance";
import { InstanceContextProvider } from "../context/InstanceContext";
import ViewEmailStatistics from "../components/statistic/ViewEmailStatistic";
import ViewEmailListStatistics from "../components/statistic/ViewEmailListStatistics";
import React from 'react';

const Dashboard = () => {
  return (
    <>
      <EmailListContextProvider>
        <Row xs={true} style={{ flexWrap: "nowrap"}}>
          <Sidebar />
          <Switch>  
            <ImportContextProvider>
            <InstanceContextProvider>
            <GuideContextProvider>
                <PrivateRoute path="/dashboard/view_lists">
                  <ViewEmailLists />
                  <PrivateRoute path="/dashboard/view_lists/create_list">
                    <CreateEmailList />
                  </PrivateRoute>
                </PrivateRoute>
                <PrivateRoute path="/dashboard/emails/:listId">
                  <ViewEmails />
                  <PrivateRoute path="/dashboard/emails/:listId/edit/:emailId">
                    <ViewUser />
                  </PrivateRoute>
                  <PrivateRoute path="/dashboard/emails/:listId/add">
                    <CreateEmail />
                  </PrivateRoute>
                </PrivateRoute>
              <PrivateRoute path="/dashboard/instance">
                <InstancePage />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/instances/view">
                <ViewInstances />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/instances/view/:instanceId">
                <ViewInstanceStatistics />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/instances/view/:instanceId/edit">
                <EditInstance />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/instances/statistics">
                <ViewInstanceStatistics from="sidebar" />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/email_list/statistics">
                <ViewEmailListStatistics from="sidebar" />
              </PrivateRoute>
              <PrivateRoute exact path="/dashboard/email/statistics">
                <ViewEmailStatistics from="sidebar" />
              </PrivateRoute>
            </GuideContextProvider>  
            </InstanceContextProvider>
            </ImportContextProvider>
          </Switch>
        </Row>
      </EmailListContextProvider>
      <Background source={Typing} />
      <RemoveScrollBar />
    </>
  );
};

export default Dashboard;
