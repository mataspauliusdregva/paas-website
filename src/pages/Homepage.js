import { Row } from "react-bootstrap";
import { RemoveScrollBar } from "react-remove-scroll-bar";
import HomepageDescription from "../components/page-content/background/Welcome";
import HomepageAnimation from "../components/page-content/background/Animation";
import Background from "../components/page-content/background/Background";
import Typing from "../components/resources/video/typing.mp4";
import React from 'react';

const Homepage = (props) => {
  return (
    <>
      <Row>
        <HomepageDescription />
        <HomepageAnimation />
      </Row>
      <Background source={Typing}/>
      <RemoveScrollBar/>
    </>
  );
};

export default Homepage;
