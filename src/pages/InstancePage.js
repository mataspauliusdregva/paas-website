import { useState } from "react";
import CreateInstanceMain from "../components/instance/create/CreateInstanceMain";
import PrivateRoute from "../utils/PrivateRoute";
import { TourProvider } from "@reactour/tour";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import CreateInstanceManually from "../components/instance/create/CreateInstanceManually";
import {
  stepsGenerate,
  stepsManual,
  stepsTemplate,
} from "../components/page-content/tour/TourStyles";
import classes from "../components/page-content/tour/Tour.module.css";
import CreateInstanceGenerate from "../components/instance/create/CreateInstanceGenerate";
import CreateInstanceTemplate from "../components/instance/create/CreateInstanceTemplate";
import React from 'react';

const InstancePage = () => {
  const [state, setState] = useState(false);
  const [enableLink, setEnableLink] = useState(false);
  const [message, setMessage] = useState([]);
  const [generatedMessage, setGeneratedMessage] = useState([]);
  const [template, setTemplate] = useState({});
  const [link, setLink] = useState([]);
  const [title, setTitle] = useState("");
  const [type, setType] = useState("");
  const [url, setUrl] = useState("")

  const switchState = () => setState((prevState) => !prevState);
  const handleEnableLink = (bool) => setEnableLink(bool);
  const getMessage = (data) => setMessage(data);
  const getGeneratedMessage = (data) => setGeneratedMessage(data)
  const getTemplate = (data) => setTemplate(data);
  const getLink = (data) => setLink(data);

  const handleType = (type) => {
    setType(type);
  };

  const resetType = () => {
    setType("");
  };

  return (
    <>
      <PrivateRoute path="/dashboard/instance/create">
        <CreateInstanceMain
          state={state}
          enable={handleEnableLink}
          switchState={switchState}
          message={message}
          generatedMessage = {generatedMessage}
          template={template}
          link={link}
          title={title}
          type={type}
          resetType={resetType}
          url = {url}
        />
      </PrivateRoute>
      {enableLink ? (
       <>
          <PrivateRoute path="/dashboard/instance/create/manual">
            <TourProvider steps={stepsManual} className={classes.tour}>
              <CreateInstanceManually
                switchState={switchState}
                message={getMessage}
                title={setTitle}
                choice={handleType}
                url= {setUrl}
              />
            </TourProvider>
          </PrivateRoute>
          <PrivateRoute path="/dashboard/instance/create/generate">
            <TourProvider steps={stepsGenerate} className={classes.tour}>
              <CreateInstanceGenerate
                switchState={switchState}
                generatedMessage={getGeneratedMessage}
                title={setTitle}
                choice={handleType}
                url= {setUrl}
              />
            </TourProvider>
          </PrivateRoute>
          <PrivateRoute path="/dashboard/instance/create/template">
            <TourProvider steps={stepsTemplate} className={classes.tour}>
              <CreateInstanceTemplate
                switchState={switchState}
                template={getTemplate}
                link={getLink}
                choice={handleType}
                url= {setUrl}
                title={setTitle}
              />
            </TourProvider>
          </PrivateRoute>
          </>
      ) : (
        <Redirect to="/dashboard/instance/create" />
      )}
    </>
  );
};

export default InstancePage;
