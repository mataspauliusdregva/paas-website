import ProfileForm from "../components/profile/EditProfileForm";
import Background from "../components/page-content/background/Background";
import Typing from "../components/resources/video/typing.mp4";
import React from 'react';

const ProfilePage = () => {
  return (
    <>
      <ProfileForm />
      <Background source={Typing}/>
    </>
  );
};

export default ProfilePage;
