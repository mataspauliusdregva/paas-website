import AuthForm from "../components/auth/AuthForm";
import Layout from "../components/layout/Layout";
import Background from "../components/page-content/background/Background";
import Typing from "../components/resources/video/typing.mp4";
import React from 'react';

const AuthPage = () => {
  return (
    <Layout>
      <AuthForm />
      <Background source={Typing}/>
    </Layout>
  );
};

export default AuthPage;
